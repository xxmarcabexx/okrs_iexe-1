$(document).ready(function () {
	$("#plan").focus(function () {
		$("#msj_plan").hide();
		$("#plan").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#descripcion").focus(function () {
		$("#msj_descripcion").hide();
		$("#descripcion").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});


	$("#btnActualizar").click(function () {
		var finalizar = 1;

		if ($("#descripcion").val() == '') {
			$("#msj_descripcion").html("Debe ingresar la descripcion del plan");
			$("#descripcion").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#descripcion").val() != '') {
			var descripcion = $("#descripcion").val();
		}
		if ($("#plan").val() == '') {
			$("#msj_plan").html("Debe ingresar el nombre del plan");
			$("#plan").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#plan").val() != '') {
			var plan = $("#plan").val();
		}

		if (finalizar == 0) {
			return false;
		} else if (finalizar == 1) {
			$.ajax({
				url: '../PlanesController/update/'+$("#idMv").val(),
				data: {
					mv: plan,
					descripcion: descripcion
				},
				type: 'POST',
				success: function (response) {
					if (response == 1) {
						alert("Los registros han sido guardados correctamente");
						window.location = '../lista_planes';
					} else if (response == 0) {
						alert("Ubo un error");
					}
				}
			});
		}
	});

});
