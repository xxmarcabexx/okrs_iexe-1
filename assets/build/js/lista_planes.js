function editarPlan(idObjetivo) {
	window.location = 'edita_plan/' + idObjetivo;
}

$(document).ready(function () {

	idAnexo = 0;

	$(".enviarChat").click(function () {
		var idPlan = $(this).attr("text");
		var fechaHoy = $("#fechaHoy").val();
		var usuarioLogueado = $("#usuarioLogueado").val();
		var nombreLogueado = $("#nombreLogueado").val();
		var men = $("#mensajeChat" + idPlan).val();
		var mensaje = '' +
			'<li>' +
			'<div class="col-md-10 mensajeChatDer">' +
			'<label>'+nombreLogueado+'('+usuarioLogueado+')</label>' +
			'<p>' + men + '</p>' +
			'<small class="col-md-12 fechamen text-right">'+fechaHoy+'</small>' +
			'</div>' +
			'</li>';
		console.log("hasta aca llegamos");
		$("#chatPlan" + idPlan).append(mensaje);
		$("#PlanChat" + idPlan).animate({scrollTop: $('#PlanChat'+idPlan)[0].scrollHeight}, 1000);
		$("#mensajeChat").val('');

		$.ajax({
			url: 'ChatController/insert',
			dataType: "json",
			data: {
				mensaje: men,
				idUsuario: $("#usuarioLogueado").val(),
				tipo: 'plan',
				idTipo: idPlan
			},
			type: 'POST',
			success: function (response) {
				$("#mensajeChat" + idPlan).val('')
			}
		});

	});




	$(".iconito").click(function () {
		if($(this).hasClass('fa fa-arrow-circle-down iconito')){
			$(this).removeClass('fa fa-arrow-circle-down iconito');
			$(this).addClass('fa fa-arrow-circle-up iconito');
		}else if($(this).hasClass('fa fa-arrow-circle-up iconito')){
			$(this).removeClass('fa fa-arrow-circle-up iconito');
			$(this).addClass('fa fa-arrow-circle-down iconito');
		}

	});

	$(".eliminaPlan").click(function () {
		//Mostramos modal de validacion
		$("#leyendaEliminar").text('Esta apunto de eliminar un plan')
		$('#modalClave').modal();
		planElimina = $(this).val();
	});

	$("#validaClave").click(function () {
		var clave = $("#contraseña").val();
		if (clave != '') {
			$.ajax({
				url: 'UsuariosController/getUserByClaveUser',
				data: {
					user: $("#usuarioLogueado").val(),
					clave: clave
				},
				type: 'POST',
				success: function (response) {
					if (response == 1) {
						$.ajax({
							url: 'elimina_plan/' + planElimina,
							data: {},
							type: 'POST',
							success: function (response) {
								if (response != 0) {
									$("#listaPlanes" + planElimina).hide();
									$('#modalClave').modal('toggle');
								} else if (response == 0) {
									alert("Ubo un error");
								}
							}
						});

					} else if (response == 0) {
						alert("Ubo un error");
					}
				}
			});
		}
	});


	$("#descripcionavance").focus(function () {
		$("#msj_descripcionavance").hide();
		$("#descripcionavance").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});

	$("#avance").focus(function () {
		$("#msj_avance").hide();
		$("#avance").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});

	$(".updateIndicador").click(function () {
		idIndicadorUpdate = $(this).val();
		$.ajax({
			url: 'IndicadoresTempController/validaAprobado',
			data: {
				idIndicador: idIndicadorUpdate,
			},
			type: 'POST',
			success: function (response) {
				console.log(response);
				if(response!=0) {
					$('#modalIndicador').modal();
				}else if(response==0){
					$('#updateCancel').modal();


				}
			}
		});
	});

	$(".updateAvance").click(function () {
        $("#avance").val('');
        $("#descripcionavance").val('');
		idIndicador = $(this).val();
		$.ajax({
			url: 'BitacoraIndicadorController/validaAprobado',
			data: {
				idIndicador: idIndicador,
			},
			type: 'POST',
			success: function (response) {
				if (response == 1) {
					$.ajax({
						url: 'IndicadoresController/getById',
						data: {
							idIndicador: idIndicador,
						},
						type: 'POST',
						success: function (response) {
							if (response != 0) {
								indicador = JSON.parse(response);
								console.log(indicador);
                                $("#tituloIndicador").html(indicador[0].nombreIndicador);
                                $("#tipometrica").html(indicador[0].nombreIndicador);
								$("#actual").html('Último avance : ' + indicador[0].avance);
								$("#rango").html('El rango debe de ser entre (' + indicador[0].inicio + ')  y (' + indicador[0].final + ')');
								$('#myModal').modal();
							} else if (response == 0) {
								alert("Ubo un error");
							}
						}
					});
				} else if (response == 0) {
					$('#aprobadoCancel').modal();
				}
			}
		});
	});

	/*$("#avance").blur(function () {
		$("#avancet").val(parseInt($(this).val())+ parseInt(indicador[0].avance));
    });*/


	$("#btnAceptar").click(function () {
		var avanza = 1;
		var anexos = new Array();
		var i = 1;
		var conArchivos = 0;
		if ($("#avance").val() == '') {
			$("#msj_avance").show();
			$("#msj_avance").html("Debe ingresar el avance");
			$("#avance").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#avance").val() != '') {
			if ((parseInt($("#avance").val()) > parseInt(indicador[0].inicio)) &&
				(parseInt($("#avance").val()) <= parseInt(indicador[0].final)) &&
				(parseInt($("#avance").val()) > parseInt(indicador[0].avance))
			) {
				var avance = $("#avance").val();
			} else {
				$("#msj_avance").show();
				$("#msj_avance").html("El avance debe ser mayor al avance anterior, mayor al valor inicial y menor o igual al la medición final");
				avanza = 0;
			}
		}

		if ($("#descripcionavance").val() == '') {
			$("#msj_descripcionavance").show();
			$("#msj_descripcionavance").html("Debe ingresar el avance");
			$("#descripcionavance").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#descripcionavance").val() != '') {
			var descripcionavance = $("#descripcionavance").val();
		}

		i = 1;
		$("#formAnexo").find('input:text').each(function () {
			var archivo = $(this).val();
			anexos.push(archivo);
			i++;
		});
		if (avanza == 1) {
			$.ajax({
				url: 'BitacoraIndicadorController/insert',
				data: {
					idIndicador: idIndicador,
					descripcion: descripcionavance,
					ultimoAvance: indicador[0].avance,
					avance: avance,
					aprobado: 0,
					user: $("#usuarioLogueado").val(),
					archivos: anexos
				},
				type: 'POST',
				success: function (response) {
					if (response > 0) {
						//window.location="lista_objetivos";
						$('#myModal').modal('toggle');
						$("#marcadorAvance"+idIndicador).html(' ( '+response+'% sin autorizar )');
						$("#marcadorAvance"+idIndicador).css({'color': 'red'})
						$("#uAvance"+idIndicador).hide();
					} else if (response == 0) {
						alert("Ubo un error");
					}
				}
			});
		}
	});


	$(".autorizaAvance").click(function () {
		botonDesaparecer = $(this);
		idBitacora = $(this).val();
		$.ajax({
			url: 'BitacoraIndicadorController/getById',
			data: {
				idBitacora: idBitacora,
			},
			type: 'POST',
			success: function (response) {
				if (response != 0) {
					$("#tablaAnexos").empty();

					var count=0;
					bitacora = JSON.parse(response);
					console.log(bitacora);
                    indicadorTa
                    $("#indicadorTa").html(bitacora[0].tituloInd);
                    $("#mensajeValida").html('Último avance elaborado por: ' +bitacora[0].capturista);
					$("#capturista").html(bitacora[0].capturista);
					$("#avanceValida").html('Valor: ' + bitacora[0].avance);
					$("#descripcionValida").html('Descripción: ' + bitacora[0].descripcion);

					$('#modalValida').modal();
					for(count=0; count<bitacora[0].anexos.length;count++){
						var fila = "<tr><td><a href='pdfstempInd/"+bitacora[0].anexos[count].file+"' target='_blank'> <i class='fa fa-file-pdf-o fa-2x' style='color: red;'></i> "+bitacora[0].anexos[count].file+"</a> </td></tr>"
						$("#tablaAnexos").append(fila);
					}
				} else if (response == 0) {
					alert("Ubo un error");
				}
			}
		});
	});


	$("#btnAceptarValida").click(function () {
		$.ajax({
			url: 'BitacoraIndicadorController/aprobar',
			data: {
				idBitacora: idBitacora,
                userAprobado: $("#usuarioLogueado").val(),
			},
			type: 'POST',
			success: function (response) {
				if (response != 0) {
					$.ajax({
						url: 'IndicadoresController/editarAvance',
						data: {
							idIndicador: bitacora[0].idIndicador,
							avance: bitacora[0].avance
						},
						type: 'POST',
						success: function (response) {
							if (response != 0) {
								$('#modalValida').modal('toggle');

								var inicio = $("#indiinicio" + bitacora[0].idIndicador).text();
								var final = $("#indifinal" + bitacora[0].idIndicador).text();
								if (inicio > 0) {
									inicio = 0;
									final = final - inicio;
								}
								var porcentaje = (bitacora[0].avance * 100) / final;
								$("#indicador" + bitacora[0].idIndicador).attr("data-transitiongoal",bitacora[0].avance);
								$("#indicador" + bitacora[0].idIndicador).css("width",porcentaje + "%");
								$("#avance" + bitacora[0].idIndicador).html(bitacora[0].avance);
								$("#autorizaAvance" + bitacora[0].idIndicador).hide();
								$("#smallindicador" + bitacora[0].idIndicador).text(porcentaje + "% Completado");
								$("#marcadorAvance"+bitacora[0].idIndicador).hide();
								console.log('marcadorAvance'+bitacora[0].idIndicador);
								botonDesaparecer.hide();
								$.ajax({
									url: 'IndicadoresController/getPlanByIdIndicador',
									data: {
										idIndicador: bitacora[0].idIndicador,
									},
									type: 'POST',
									success: function (response) {
										infoGraf = JSON.parse(response);
										console.log(infoGraf);
										$("#gaugeIndi"+infoGraf[0].idMv).empty();

										g = new JustGage({
											id: 'gaugeIndi'+infoGraf[0].idMv,
											value: infoGraf[0].avanceIndicadores,
											min: 0,
											max: 100,
											titlePosition: "below",
											valueFontColor: "#3f4c6b",
											pointer: true,
											pointerOptions: {
												toplength: -15,
												bottomlength: 10,
												bottomwidth: 12,
												color: '#8e8e93',
												stroke: '#ffffff',
												stroke_width: 3,
												stroke_linecap: 'round'
											},
											relativeGaugeSize: true,

										});
									}
								});

							} else if (response == 0) {
								alert("Ubo un error");
								return 0;
							}
						}
					});
				} else if (response == 0) {
					alert("Ubo un error");
					return 0;
				}
			}
		});
	});





	$("#btnAceptarEditarIndicador").click(function () {
		var avanza = 1;
		var vinicial = $("#vinicial").val();
		var vfinal = $("#vfinal").val();
		if(vinicial>vfinal){
			$("#msj_vinicial").text('El valor inicial debe ser menor al valor final');
			avanza = 0;
		}
		if(vinicial<0){
			$("#msj_vinicial").text('El valor inicial no puede ser negativo');
			avanza = 0;
		}
		if(vfinal<0){
			$("#msj_vfinal").text('El valor final no puede ser negativo');
			avanza = 0;
		}
		if(avanza==1){
			$.ajax({
				url: 'IndicadoresTempController/insert/',
				data: {
					inicio: vinicial,
					final: vfinal,
					idIndicadores: idIndicadorUpdate,
					status: 0
				},
				type: 'POST',
				success: function (response) {
					var indic = JSON.parse(response);
					$('#modalIndicador').modal('toggle');
					$("#vinicial").val('');
					$("#vfinal").val('');
				}
			});
		}
	});

	$(".updateSiIndicador").click(function () {
		indiUpdate = $(this).val();
		$('#modalClaveEditar').modal();
	});

	$("#validaClaveEditar").click(function () {
		var clave = $("#contraseñaUpdate").val();
		if (clave != '') {
			$.ajax({
				url: 'UsuariosController/getUserByClaveUser',
				data: {
					user: $("#usuarioLogueado").val(),
					clave: clave
				},
				type: 'POST',
				success: function (response) {
					if (response == 1) {
						$.ajax({
							url: 'cambioIndicador/' + indiUpdate,
							data: {},
							type: 'POST',
							success: function (response) {
								if (response != 0) {
									window.location="lista_planes";
									// $('#modalClaveEditar').modal('toggle');
								} else if (response == 0) {
									alert("Ubo un error");
								}
							}
						});
						alert("Procedemos a hacer el cambio");

					} else if (response == 0) {
						alert("Contraseña erronea");
					}
				}
			});
		}
	});


$(".agregaAnexo").click(function () {
		idAnexo++;
		$("#formAnexo").append('' +
			'<div id="anexo' + idAnexo + '" style="height: 50px">' +
			'	<div class="col-md-8 col-sm-8 col-xs-12">' +
			'		<input type="file" class="form-control anexoFile" name="'+idAnexo+'" id="ane' + idAnexo + '">' +
			'		<span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>' +
			'	</div>' +
			'	<div class="col-md-4 text-center" style="margin-top: 5px;">' +
			'		<button class="btn btn-danger btn-xs elimAnex" value="' + idAnexo + '" >' +
			'			<i class="fa fa-trash"></i>Eliminar' +
			'       </button>' +
			'	</div>' +
			'</div>'
		);
		$(".elimAnex").click(function () {
			var valorFila = $(this).attr("value");
			var archivo = $("#ane"+valorFila).val();
			$("#anexo" + valorFila).remove();
			$.ajax({
				url: 'BitacoraIndicadorController/borrarArchivoTemporal',
				dataType: "json",
				data: {
					archivo: archivo,
				},
				type: 'POST',
				success: function (response) {}
			});

		});
	});



$("#btnFinalizar").click(function () {
		$("#formAnexo").empty();
	});

	$(document).on('change', '.anexoFile', function(e) {
		var data = new FormData();
		var lugar = $(this).attr("name");

		var inputFileImage = document.getElementById("ane" + lugar);
		var file = inputFileImage.files[0];
		data.append('ane' + lugar, file);

		var url = 'BitacoraIndicadorController/uploadFileTemp/';
		$.ajax({
			url: url,
			type: 'POST',
			contentType: false,
			data: data,
			processData: false,
			cache: false,
			success: function (response) {
				if(response!='') {
					$("#barraCarga").fadeOut(5000);
					$("#ane"+lugar).attr("type", "text");
					$("#ane"+lugar).val(response);
					$("#ane"+lugar).attr("readonly", true);
					$("#ane"+lugar).css("padding-left", "45px");
				}

			},
			xhr: function(){
				$("#barraProgresoCarga").css("width", 0 + "%");
				$("#barraCarga").show();
				var xhr = $.ajaxSettings.xhr() ;
				xhr.upload.onprogress=function (e) {
					if (e.lengthComputable) {
						var porcentaje = Math.floor((e.loaded / e.total) * 100);
						$("#barraProgresoCarga").attr("data-transitiongoal", porcentaje);
						$("#barraProgresoCarga").css("width", porcentaje + "%");
						$("#porcentajeProgreso").text(porcentaje + '%');
					}
				}
				return xhr ;
			}
		});
	});

	$("#btnNoAutorizar").click(function () {
		$("#modalValida").modal('toggle');
		$("#motivoCancel").html(bitacora[0].descripcion);
		$("#avanceDe").html(bitacora[0].capturista);
		$("#noAutorizar").modal();
	});

	$("#aceptarCancelacion").click(function(){
		var idBitacora =  bitacora[0].idBitacora;
		var motivo = $("#motivo").val();
		$.ajax({
				url: 'BitacoraIndicadorController/rechazar',
				data: {
					idBitacora: idBitacora,
					motivo: motivo,
					userNoAutorizo: $("#usuarioLogueado").val(),
				},
				type: 'POST',
				success: function (response) {
					$("#autorizaAvance" + bitacora[0].idIndicador).hide();
				}
		});
	});


	$(".avanceCancelado").click(function(){
		idIndicador = $(this).val();
		$.ajax({
			url: 'BitacoraIndicadorController/cancelado',
			data: {
				idIndicador: idIndicador,
			},
			type: 'POST',
			success: function (response) {
				if (response != 0) {
					info = JSON.parse(response);
					console.log(info);

					$("#tituloCancelado").html("Key Result Cancelado");
					$("#usuarioCancelado").html('Cancelado por: ' + info[0].userCancel);
					$("#avanceCancelado").html('Avance Cancelado : ' + info[0].avance);
					$("#motivoCancelado").html('Motivo: '+ info[0].motivo);
					$('#modalCancelado').modal();
				} else if (response == 0) {
					alert("Ubo un error");
				}
			}
		});
	});

	$("#btnAceptarCancelado").click(function(){
		var idBitacora = info[0].idBitacora;
		$.ajax({
				url: 'BitacoraIndicadorController/validaCancelado',
				data: {
					idBitacoraEnvio: idBitacora
				},
				type: 'POST',
				success: function (response) {
					if (response>0) {
						$("#modalCancelado").modal('toggle');
						$("#avanceCancelado" + info[0].idIndicador).hide();

					}else{
						alert("Ubo un error");
					}
				}
		});
	});

	$(".bitacoraLista").click(function () {
		var i = 0;
        $.ajax({
            url: 'BitacoraIndicadorController/getByIdIndicador',
            data: {
                idIndicador: $(this).val(),
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                	var anexos='';
                	var i=0;
                    var tam = 0;
                    var j=0;
                    $("#tablaBitacora tbody tr").remove();
                	var dataResponse = JSON.parse(response);
                	console.log(dataResponse);
                	$("#tIndicador").html(dataResponse[0].tituloIndicador);
                	for(i=0; i< dataResponse.length; i++ ) {
                		for (j = 0; j< dataResponse[i].adjuntos.length; j++) {
                			anexos += "<a download style='font-size: 10px;' href='pdfstempInd/"+dataResponse[i].adjuntos[j].file+"'>"+dataResponse[i].adjuntos[j].file+"</a><br><br>";
                        }
                        $("#tablaBitacora tbody").append(
                            "<tr>" +
								"<td>" +
									dataResponse[i].fecha +
								"</td>" +
                     	       	"<td>" +
                        		    dataResponse[i].descripcion +
                            	"</td>" +
								"<td>" +
									dataResponse[i].avance +
								"</td>" +
                            	"<td>" +
									dataResponse[i].avance +
								"</td>" +
                            	"<td>" +
									dataResponse[i].aprobado +
                            	"</td>" +
								"<td>" +
									dataResponse[i].user +
								"</td>" +
								"<td>" +
									dataResponse[i].motivo +
								"</td>" +
								"<td>" +
									anexos+
								"</td>" +
                            "</tr>"
                        );
                    }
					$("#modalBitacora").modal();
                } else if (response == 0) {
                    $("#tablaBitacora tbody tr").remove();
                    $("#indicadorT").html("Sin bitácora");
                    $("#tIndicador").html("");
                    $("#modalBitacora").modal();
                }
            }
        });
    });


});
