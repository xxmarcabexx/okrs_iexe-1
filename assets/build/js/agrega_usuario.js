function showStatus(online) {
	const statusEl = document.querySelector('.onlineState');

	if (online) {
		statusEl.classList.remove('warning');
		statusEl.classList.add('success');
		statusEl.innerText = ` (Online)`;
	} else {
		statusEl.classList.remove('success');
		statusEl.classList.add('warning');
		statusEl.innerText = ` (Ofline)`;
	}
}

window.addEventListener('load', () => {
	// 1st, we set the correct status when the page loads
	navigator.onLine ? showStatus(true) : showStatus(false);

	// now we listen for network status changes
	window.addEventListener('online', () => {
		showStatus(true);
	});

	window.addEventListener('offline', () => {
		showStatus(false);
	});
});

$(document).ready(function () {
	$(function (a) {
		a.fn.validaCaracter = function (b) {
			a(this).on({
				keypress: function (a) {
					var c = a.which, d = a.keyCode, e = String.fromCharCode(c).toLowerCase(), f = b;
					(-1 != f.indexOf(e) || 9 == d || 37 != c && 37 == d || 39 == d && 39 != c || 8 == d || 46 == d && 46 != c) && 161 != c || a.preventDefault()
				}
			})
		}
	});

	$('#telefono').validaCaracter('1234567890');
	$('#celular').validaCaracter('1234567890');

	$("#usuario").keyup(function(){
		if( $(this).val().length < 6){
			$("#msj_usuario").html('El usuario debe ser mayor a 6 caracteres');
		}else{
			$("#msj_usuario").html('');
			$.ajax({
				url: 'UsuariosController/getUser',
				data: {
					user: $("#usuario").val()
				},
				type: 'POST',
				success: function (response) {
					if (response == 1) {
						$("#msj_usuario").html('El usuario ya existe');
					}
				}
			});
		}
	});

	$("#clave").keyup(function(){
		if( $(this).val().length < 6){
			$("#msj_clave").html('La clave debe ser mayor a 6 caracteres');
		}else{
			$("#msj_clave").html('');
		}
	});
	$("#clave_valida").keyup(function(){
		console.log($(this).val() + $("#clave").val());
		if( $(this).val() != $("#clave").val()){
			$("#msj_clave_valida").html('Las claves no coiciden');
		}else{
			$("#msj_clave_valida").html('');
		}
	});

	/*$("#medicioncomienzo").focus(function () {
		$("#msj_comienzo").hide();
		$("#medicioncomienzo").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#medicionfinal").focus(function () {
		$("#msj_tope").hide();
		$("#medicionfinal").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});

	$("#orden").focus(function () {
		$("#msj_orden").hide();
		$("#orden").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});

	$("#descripcionkr").focus(function () {
		$("#msj_descripcionkr").hide();
		$("#descripcionkr").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	*/

	$("#tipo").change(function () {
		if($(this).val()==4){
            $("#acuerdosV").prop( "checked" , false);
            $('#grupoKr').show();
			$("#grupoTag").show();
			$("#grupoPlanes").show();
			$("#grupoTagPl").show()
		}else if($(this).val()==3){
            $("#acuerdosV").prop( "checked" , false);
            $("#grupoPlanes").show();
            $("#grupoTagPl").show()
		}else if($(this).val()==1){
            $("#acuerdosV").prop( "checked" , true);
		}else{
            $("#acuerdosV").prop( "checked" , false);
            $('#grupoKr').hide();
            $("#grupoTag").hide();
            $("#grupoPlanes").hide();
            $("#grupoTagPl").hide();
		}
	});


	$('#kr').change(function(){
		var agrega = 1;
		var opcion = $('select[name="kr"] option:selected').text();
		var valorOpcion = $('select[name="kr"] option:selected').val();
		if(valorOpcion!=0) {
			$("#tagkr button").each(function () {
				if (valorOpcion == $(this).val()) {
					agrega = 0;
				}
			});
			if (agrega == 1) {
				$("#tagkr").append('<button value="' + valorOpcion + '" class="krbtn btn btn-success btn-xs" style="margin-top: 15px !important;">' + opcion + '<span class="fa fa-chevron-down"></span></button>');
			}
		}
	});
	$(document).on('click', '.krbtn', function(e) {
			$(this).remove();
	});




	$('#pl').change(function(){
		var agrega = 1;
		var opcion = $('select[name="pl"] option:selected').text();
		var valorOpcion = $('select[name="pl"] option:selected').val();
		if(valorOpcion!=0) {
			$("#tagpl button").each(function () {
				if (valorOpcion == $(this).val()) {
					agrega = 0;
				}
			});
			if (agrega == 1) {
				$("#tagpl").append('<button value="' + valorOpcion + '" class="plbtn btn btn-success btn-xs" style="margin-top: 15px !important;">' + opcion + '<span class="fa fa-chevron-down"></span></button>');
			}
		}
	});
	$(document).on('click', '.plbtn', function(e) {
		$(this).remove();
	});



	$("#btnGuardar").click(function () {
		var finalizar = 1;

		var keyresult = Array();
		var planes = Array();
		var i=0;
		$("#tagkr button").each(function(){
			var bandera=1;
			for(i=0; i<keyresult.length; i++){
				if($(this).val()==keyresult[i]){
					bandera=0;
				}
			}
			if(bandera==1){
				keyresult.push($(this).val());
			}
		});

		$("#tagpl button").each(function(){
				planes.push($(this).val());
		});

		if ($("#usuario").val() == '') {
			$("#msj_usuario").html("Debe ingresar el usuario");
			$("#usuario").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#usuario").val() != '') {
			var usuario = $("#usuario").val();
		}

		if ($("#clave").val() == '') {
			$("#msj_clave").html("Debe ingresar la contraseña del usuario");
			$("#clave").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#clave").val() != '') {
			var clave = $("#clave").val();
		}

		if ($("#clave_valida").val() == '') {
			$("#msj_clave_valida").html("Debe ingresar de nuevo la contraseña");
			$("#clave_valida").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#clave_valida").val() != '') {
			var clave_valida = $("#clave_valida").val();
		}

		if ($("#tipo").val() == '' || $("#tipo").val() == 0) {
			$("#msj_tipo").html("Debe seleccionar el tipo de usuario");
			$("#tipo").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#tipo").val() != '') {
			var tipo = $("#tipo").val();
		}

		if ($("#nombre").val() == '') {
			$("#msj_nombre").html("Debe ingresar el nombre del usuario");
			$("#nombre").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#nombre").val() != '') {
			var nombre = $("#nombre").val();
		}

		if ($("#apellidoP").val() == '') {
			$("#msj_apellidoP").html("Debe ingresar el apellido paterno del usuario");
			$("#apellidoP").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#apellidoP").val() != '') {
			var apellidoP = $("#apellidoP").val();
		}


		if ($("#apellidoM").val() == '') {
			$("#msj_apellidoM").html("Debe ingresar el apellido materno del usuario");
			$("#apellidoM").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#apellidoM").val() != '') {
			var apellidoM = $("#apellidoM").val();
		}

		if ($("#telefono").val() == '') {
			$("#msj_telefono").html("Debe ingresar el telefono del usuario");
			$("#telefono").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#telefono").val() != '') {
			var telefono = $("#telefono").val();
		}

		if ($("#celular").val() == '') {
			$("#msj_celular").html("Debe ingresar el celular del usuario");
			$("#celular").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#celular").val() != '') {
			var celular = $("#celular").val();
		}

		if ($("#puesto").val() == '') {
			$("#msj_puesto").html("Debe ingresar el puesto del usuario");
			$("#puesto").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#puesto").val() != '') {
			var puesto = $("#puesto").val();
		}

		if ($("#correo").val() == '') {
			$("#msj_correo").html("Debe ingresar un correo");
			$("#correo").css({"border": "1px solid red", "border-radius": "4px"});
			finalizar = 0;
		} else if ($("#correo").val() != '') {
			var correo = $("#correo").val();
		}

		if(clave != clave_valida){
			$("#msj_clave_valida").html("La clave debe ser identica");
			finalizar=0;
		}

        if ($("#acuerdosV").is(':checked')) {
            var acuerdos = 1;
        } else  {
            var acuerdos = 0;
        }

		if (finalizar == 0) {
			return false;
		} else if (finalizar == 1) {
			$.ajax({
				url: 'UsuariosController/insert',
				data: {
					user: usuario,
					acuerdos: acuerdos,
					clave: clave,
					idRol: tipo,
					nombre: nombre,
					apellidoP: apellidoP,
					apellidoM: apellidoM,
					telefono: telefono,
					celular: celular,
					puesto: puesto,
					correo: correo,
					kr: keyresult,
					planes: planes

				},
				type: 'POST',
				success: function (response) {
					if (response == 1) {
                        alert("Los registros han sido guardados correctamente");
                        window.location='lista_usuarios';
					} else if (response == 0) {
						alert("Ubo un error");
					}
				},
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr() ;
                    xhr.onloadstart = function(e) {
                        $("#fondoLoader").show();
                    };
                    xhr.upload.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(2000);
                    }
                    return xhr ;
                }
			});
		}
	});


	$("#btnNkr").click(function () {
		$('#myModal').modal('hide');
		$("#medicioncomienzo").val('');
		$("#medicionfinal").val('');
		$("#descripcionkr").val('');
		$("input[name='optionsRadios']:checked").prop('checked', false);
		$("#orden").val(0);
	});

	$(".btnFinalizar").click(function () {
		window.location = 'lista_usuarios';
	});



});
