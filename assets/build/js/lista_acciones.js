function showStatus(online) {
    const statusEl = document.querySelector('.onlineState');

    if (online) {
        statusEl.classList.remove('warning');
        statusEl.classList.add('success');
        statusEl.innerText = ` (Online)`;
    } else {
        statusEl.classList.remove('success');
        statusEl.classList.add('warning');
        statusEl.innerText = ` (Ofline)`;
    }
}

window.addEventListener('load', () => {
    // 1st, we set the correct status when the page loads
    navigator.onLine ? showStatus(true) : showStatus(false);

// now we listen for network status changes
window.addEventListener('online', () => {
    showStatus(true);
});

window.addEventListener('offline', () => {
    showStatus(false);
});
});


function editarObjetivo(idObjetivo) {
    window.location = 'edita_objetivo/' + idObjetivo;
}

function agregarKey(idObjetivo) {
    window.location = 'agrega_key/' + idObjetivo;
}

function eliminarObjetivo(idObjetivo) {
    window.location = 'elimina_objetivo/' + idObjetivo;
}


$(document).ready(function () {
    /*window.addEventListener('online', () => {
        // 🦄🎊🔥 we're back online!
        alert("");
    });

    window.addEventListener('offline', () => {
        // 👨‍💻🙅‍😱 oh no!
    });*/

    $(".iconito").click(function () {
        if ($(this).hasClass('fa fa-arrow-circle-down iconito')) {
            $(this).removeClass('fa fa-arrow-circle-down iconito');
            $(this).addClass('fa fa-arrow-circle-up iconito');
        } else if ($(this).hasClass('fa fa-arrow-circle-up iconito')) {
            $(this).removeClass('fa fa-arrow-circle-up iconito');
            $(this).addClass('fa fa-arrow-circle-down iconito');
        }
    });

    $("#nuevaDescripcionKr").focus(function () {
        $("#msj_nDescripcion").fadeOut(500);
    });
    $("#nuevoInicio").focus(function () {
        $("#msj_nInicio").fadeOut(500);
    });
    $("#nuevoFinal").focus(function () {
        $("#msj_nFinal").fadeOut(500);
    });
    $("#nuevoAvance").focus(function () {
        $("#msj_nAvance").fadeOut(500);
    });

    idAnexo = 0;
    eKr = 0;
    eObj = 0;

    $(".editarKr").click(function () {
        krEdit = $(this).val();
        $.ajax({
            url: "KeyResultController/getById",
            data:{
                idKr: krEdit
            },
            type: 'POST',
            success:function (response) {
                var datosKr = JSON.parse(response);
                console.log(datosKr);
                $("#nuevaDescripcionKr").val(datosKr[0].descripcion);
                $("#nuevoInicio").val(datosKr[0].medicioncomienzo);
                $("#nuevoFinal").val(datosKr[0].medicionfinal);
                $("#nuevoAvance").val(datosKr[0].avance);
                $("#modalEditarKr").modal();
            }
        });

    });
    $("#btnAceptarEdicionKr").click(function () {
        var avanza = 1;
        krNew = new Object();

        if ($("#nuevaDescripcionKr").val() != '') {
            var descripcion = $("#nuevaDescripcionKr").val()
        } else {
            avanza = 0;
            $("#msj_nDescripcion").show();
            $("#msj_nDescripcion").html("Debe ingresar la descripción de la Key Result");
            $("#msj_nDescripcion").css({'color': 'red'});
        }

        if (($("#nuevoInicio").val() != '')) {
            var medicioncomienzo = $("#nuevoInicio").val();
        } else {
            avanza = 0;
            $("#msj_nInicio").show();
            $("#msj_nInicio").html("Debe ingresar el valor inicial");
            $("#msj_nInicio").css({'color': 'red'});
        }
        if (($("#nuevoFinal").val() != '')) {
            var medicionfinal = $("#nuevoFinal").val()
        } else {
            avanza = 0;
            $("#msj_nFinal").show();
            $("#msj_nFinal").html("Debe ingresar el valor final");
            $("#msj_nFinal").css({'color': 'red'});
        }

        if ($("#nuevoAvance").val() != '') {
            var avance = $("#nuevoAvance").val()
        } else {
            avanza = 0;
            $("#msj_nAvance").show();
            $("#msj_nAvance").html("Debe ingresar el valor del avance");
            $("#msj_nAvance").css({'color': 'red'});
        }
        if (parseInt(medicioncomienzo) > parseInt(medicionfinal)) {
            $("#msj_nInicio").show();
            $("#msj_nInicio").html("El valor inicial debe ser menor al valor final");
            $("#msj_nInicio").css({'color': 'red'});
            avanza = 0;
        }
        if (parseInt(medicioncomienzo) > parseInt(medicionfinal)) {
            $("#msj_nFinal").show();
            $("#msj_nFinal").html("El valor final debe ser mayor al valor inicial");
            $("#msj_nFinal").css({'color': 'red'});
            avanza = 0;
        }
        if (parseInt(avance) < parseInt(medicioncomienzo) || parseInt(avance) > parseInt(medicionfinal)) {
            $("#msj_nAvance").show();
            $("#msj_nAvance").html("El avance debe estar entre el valor inicial y el valor final");
            $("#msj_nAvance").css({'color': 'red'});
            avanza = 0;
        }
        if(avanza == 1){
            $("#modalEditarKr").modal('toggle');
            $('#modalClaveEditarKr').modal();
            krNew.descripcion = descripcion;
            krNew.medicioncomienzo = medicioncomienzo;
            krNew.medicionfinal = medicionfinal;
            krNew.avance = avance;
            krNew.idKeyResult = krEdit;

        }
    });
    $("#btnCerrarEdicion").click(function () {
        $("#nuevaDescripcionKr").val('');
        $("#nuevoInicio").val('');
        $("#nuevoFinal").val('');
        $("#nuevoAvance").val('');
    });
    $("#cerraClaveEdit").click(function () {
        $("#nuevaDescripcionKr").val('');
        $("#nuevoInicio").val('');
        $("#nuevoFinal").val('');
        $("#nuevoAvance").val('');
    });

    $("#validaClaveEdit").click(function () {
        $.ajax({
            url: "KeyResultController/edit",
            data: {
                objeto : krNew
            },
            type: 'POST',
            success: function (response) {
                var respuesta = JSON.parse(response);
                console.log(respuesta);

                $("#descripcion"+krNew.idKeyResult).html(krNew.descripcion);
                $("#avance"+krNew.idKeyResult).html(krNew.avance);
                $("#keyresultinicio"+krNew.idKeyResult).html(krNew.medicioncomienzo);
                $("#keyresultfinal"+krNew.idKeyResult).html(krNew.medicionfinal);
                $("#small"+krNew.idKeyResult).html(respuesta.avancePorcentaje.toFixed(2)+"%");
                $('#modalClaveEditarKr').modal('toggle');

                $("#gauge" + respuesta.idObjetivo).empty();

                g = new JustGage({
                    id: 'gauge' + respuesta.idObjetivo,
                    value: respuesta.porcentajeObjetivo.toFixed(2),
                    min: 0,
                    max: 100,
                    titlePosition: "below",
                    valueFontColor: "#3f4c6b",
                    pointer: true,
                    pointerOptions: {
                        toplength: -15,
                        bottomlength: 10,
                        bottomwidth: 12,
                        color: '#8e8e93',
                        stroke: '#ffffff',
                        stroke_width: 3,
                        stroke_linecap: 'round'
                    },
                    relativeGaugeSize: true,

                });




            }

        });
    });




    $(".eliminaObjetivo").click(function () {
        //Mostramos modal de validacion
        $("#leyendaEliminar").text('Esta apunto de eliminar un objetivo')
        $('#modalClave').modal();
        objetivoElimina = $(this).val();
        eKr = 0;
        eObj = 1;
    });
    $(".eliminaKr").click(function () {
        //Mostramos modal de validacion
        $("#leyendaEliminar").text('Esta apunto de eliminar un key result')
        $('#modalClave').modal();
        krElimina = $(this).val();
        eKr = 1;
        eObj = 0;
    });


    $("#validaClave").click(function () {
        var clave = $("#contraseña").val();
        if (clave != '') {
            $.ajax({
                url: 'UsuariosController/getUserByClaveUser',
                data: {
                    user: $("#usuarioLogueado").val(),
                    clave: clave
                },
                type: 'POST',
                success: function (response) {
                    if (response == 1) {
                        if (eObj == 1) {
                            $.ajax({
                                url: 'elimina_objetivo/' + objetivoElimina,
                                data: {},
                                type: 'POST',
                                success: function (response) {
                                    if (response != 0) {
                                        $(".listaObjetivos" + objetivoElimina).hide();
                                        $('#modalClave').modal('toggle');
                                    } else if (response == 0) {
                                        alert("Ubo un error");
                                    }
                                }
                            });
                        } else if (eKr == 1) {
                            $.ajax({
                                url: 'elimina_kr/' + krElimina,
                                data: {},
                                type: 'POST',
                                success: function (response) {
                                    var arrayObj = JSON.parse(response);
                                    console.log(arrayObj);
                                    if (response != 0) {
                                        $(".listaKr" + krElimina).remove();

                                        $("#gauge" + arrayObj.idObjetivo).empty();

                                        g = new JustGage({
                                            id: 'gauge' + arrayObj.idObjetivo,
                                            value: arrayObj.porcentajeObjetivo.toFixed(2),
                                            min: 0,
                                            max: 100,
                                            titlePosition: "below",
                                            valueFontColor: "#3f4c6b",
                                            pointer: true,
                                            pointerOptions: {
                                                toplength: -15,
                                                bottomlength: 10,
                                                bottomwidth: 12,
                                                color: '#8e8e93',
                                                stroke: '#ffffff',
                                                stroke_width: 3,
                                                stroke_linecap: 'round'
                                            },
                                            relativeGaugeSize: true,

                                        });


                                        $('#modalClave').modal('toggle');
                                    } else if (response == 0) {
                                        alert("Ubo un error");
                                    }
                                }
                            });
                        }

                    } else if (response == 0) {
                        alert("Ubo un error");
                    }
                }
            });
        }
    });


    $("#descripcionavance").focus(function () {
        $("#msj_descripcionavance").hide();
        $("#descripcionavance").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
    });

    $("#avance").focus(function () {
        $("#msj_avance").hide();
        $("#avance").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
    });


    $(".avanceCancelado").click(function () {
        idKr = $(this).val();
        $.ajax({
            url: 'BitacoraKrController/cancelado',
            data: {
                idKr: idKr,
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    info = JSON.parse(response);
                    console.log(info);

                    $("#tituloCancelado").html("Key Result Cancelado");
                    $("#usuarioCancelado").html('Cancelado por: ' + info[0].userCancel);
                    $("#avanceCancelado").html('Avance Cancelado : ' + info[0].avance);
                    $("#motivoCancelado").html('Motivo: ' + info[0].motivo);
                    $('#modalCancelado').modal();
                } else if (response == 0) {
                    alert("Ubo un error");
                }
            }
        });
    });

    $(".updateAvance").click(function () {
        $("#formAnexo").empty();

        $("#avance").val('');
        $("#descripcionavance").val('');
        idAccion = $(this).val();
        $.ajax({
            url: 'BitacoraAccionesController/validaAprobado',
            data: {
                idAccion: idAccion,
            },
            type: 'POST',
            success: function (response) {
                if (response == 1) {
                    $.ajax({
                        url: 'AccionesController/getById',
                        data: {
                            idAccion: idAccion,
                        },
                        type: 'POST',
                        success: function (response) {
                            if (response != 0) {
                                kr = JSON.parse(response);
                                console.log(kr);
                                if (kr[0].metrica == "Dicotomica") {
                                    $("#tipometricaDic").html('Métrica: ' + kr[0].metrica)
                                    $('#myModalDic').modal();
                                } else {
                                    $("#tituloKr").html(kr[0].descripcion);
                                    $("#tipometrica").html('Métrica: ' + kr[0].metrica);
                                    $("#actual").html('Último avance : ' + kr[0].avance);
                                    $("#rango").html('El rango debe de ser entre (' + kr[0].medicioncomienzo + ')  y (' + kr[0].medicionfinal + ')');
                                    $('#myModal').modal();
                                }
                            } else if (response == 0) {
                                alert("Ubo un error");
                            }
                        }
                    });
                } else if (response == 0) {
                    $('#aprobadoCancel').modal();
                }
            }
        });
    });


    $("#btnAceptar").click(function () {
        $("#descripcionavanceDic").val('');
        var anexos = new Array();
        var i = 1;
        var conArchivos = 0;


        $("#formAnexo").find('input:file').each(function () {
            $(this).prop("id", "ane" + i);
            i++;
        });

        var avanza = 1;
        if ($("#avance").val() == '') {
            $("#msj_avance").show();
            $("#msj_avance").html("Debe ingresar el avance");
            $("#avance").css({"border": "1px solid red", "border-radius": "4px"});
            avanza = 0;
        } else if ($("#avance").val() != '') {
            if ((parseInt($("#avance").val()) > parseInt(kr[0].medicioncomienzo)) &&
                (parseInt($("#avance").val()) <= parseInt(kr[0].medicionfinal)) &&
                (parseInt($("#avance").val()) > parseInt(kr[0].avance))
            ) {
                var avance = $("#avance").val();
            } else {
                $("#msj_avance").show();
                $("#msj_avance").html("Debe de ser mayor al último avance");
                $("#msj_avance").css({"color": 'red'});
                avanza = 0;
            }
        }
        if ($("#descripcionavance").val() == '') {
            $("#msj_descripcionavance").show();
            $("#msj_descripcionavance").html("Debe ingresar el avance");
            $("#descripcionavance").css({"border": "1px solid red", "border-radius": "4px"});
            avanza = 0;
        } else if ($("#descripcionavance").val() != '') {
            var descripcionavance = $("#descripcionavance").val();
        }


        i = 1;
        $("#formAnexo").find('input:text').each(function () {
            var archivo = $(this).val();
            anexos.push(archivo);
            i++;
        });
        if (avanza == 1) {
            $.ajax({
                url: 'BitacoraKrController/insert',
                data: {
                    idKeyResult: idKr,
                    descripcion: descripcionavance,
                    ultimoAvance: kr[0].avance,
                    avance: avance,
                    aprobado: 0,
                    user: $("#usuarioLogueado").val(),
                    archivos: anexos
                },
                type: 'POST',
                success: function (response) {
                    if (response != 0) {
                        var av = parseFloat(response).toFixed(2);

                        $('#myModal').modal('toggle');
                        $("#marcadorAvance" + idKr).show();
                        $("#updateAvance" + idKr).hide();
                        if($("#tipoUsuario").val()=='superadmin' ||$("#tipoUsuario").val()=='admin'){
                            $("#small" + idKr).empty();
                            $("#marcadorAvance" + idKr).html(parseFloat(av).toFixed(2)+'%');
                            $("#avance"+ idKr).html(avance);
                        }else {
                            $("#marcadorAvance" + idKr).html(' ( ' + parseFloat(av).toFixed(2) + '% sin autorizar )');
                            $("#marcadorAvance" + idKr).css({'color': 'red'});
                        }

                        $("#avance").val('');
                        $("#descripcionavance").val('');
                    } else {
                        alert("Ubo un error");
                    }
                }
            });
        }
    });


    $("#btnAceptarDic").click(function () {
        var avanza = 1;
        var dico = $("input[name='dico']:checked").val();
        if (dico != 'Si' && dico != 'No') {
            avanza = 0;
        } else {
            avance = 100;
        }
        if ($("#descripcionavanceDic").val() == '') {
            $("#msj_descripcionavanceDic").show();
            $("#msj_descripcionavanceDic").html("Debe ingresar el avance");
            $("#descripcionavance").css({"border": "1px solid red", "border-radius": "4px"});
            avanza = 0;
        } else if ($("#descripcionavanceDic").val() != '') {
            var descripcionavance = $("#descripcionavanceDic").val();
        }
        if (avanza == 1) {
            $.ajax({
                url: 'BitacoraKrController/insert',
                data: {
                    idKeyResult: idKr,
                    descripcion: descripcionavance,
                    ultimoAvance: kr[0].avance,
                    avance: avance,
                    aprobado: 0,
                    user: $("#usuarioLogueado").val()
                },
                type: 'POST',
                success: function (response) {
                    console.log(response);
                    if (response >= 1) {
                        $("#marcadorAvance" + idKr).show();
                        $("#updateAvance" + idKr).hide();
                        $("#marcadorAvance" + idKr).html(' ( ' + response + '% sin autorizar )');
                        $("#marcadorAvance" + idKr).css({'color': 'red'});
                        $("#avance").val('');
                        $("#descripcionavance").val('');
                        $("#myModalDic").modal('toggle');
                    } else if (response == 0) {
                        alert("Ubo un error");
                    }
                }
            });
        }
    });


    $(".autorizaAvance").click(function () {
        idBitacora = $(this).val();
        $.ajax({
            url: 'BitacoraKrController/getById',
            data: {
                idBitacora: idBitacora,
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $("#tablaAnexos").empty();
                    var count = 0;
                    bitacora = JSON.parse(response);
                    console.log(bitacora);
                    $("#mensajeValida").html('Último avance elaborado por: ' + bitacora[0].capturista);
                    $("#avanceValida").html('Valor: ' + bitacora[0].avance);
                    $("#descripcionValida").html('Descripción: ' + bitacora[0].descripcion);
                    $('#modalValida').modal();
                    for (count = 0; count < bitacora[0].anexos.length; count++) {
                        var fila = "<tr><td><a href='pdfstemp/" + bitacora[0].anexos[count].file + "' target='_blank'> <i class='fa fa-file-pdf-o fa-2x' style='color: red;'></i> " + bitacora[0].anexos[count].file + "</a> </td></tr>"
                        $("#tablaAnexos").append(fila);
                    }
                } else if (response == 0) {
                    alert("Ubo un error");
                }
            }
        });
    });

    $("#btnNoAutorizar").click(function () {
        $("#modalValida").modal('toggle');
        $("#motivoCancel").html(bitacora[0].descripcion);
        $("#avanceDe").html(bitacora[0].capturista);
        $("#noAutorizar").modal();
        $("#motivo").val('');
    });

    $("#btnAceptarValida").click(function () {

        $.ajax({
            url: 'BitacoraKrController/aprobar',
            data: {
                idBitacora: idBitacora,
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $.ajax({
                        url: 'KeyResultController/editarAvance',
                        data: {
                            idKeyResult: bitacora[0].idKeyResult,
                            avance: bitacora[0].avance
                        },
                        type: 'POST',
                        success: function (response) {
                            if (response != 0) {
                                $('#modalValida').modal('toggle');

                                var inicio = $("#keyresultinicio" + bitacora[0].idKeyResult).text();
                                var final = $("#keyresultfinal" + bitacora[0].idKeyResult).text();
                                if (inicio > 0) {
                                    inicio = 0;
                                    final = final - inicio;
                                }

                                var porcentaje = (bitacora[0].avance * 100) / final;
                                $("#barraProgreso" + bitacora[0].idKeyResult).attr("data-transitiongoal", bitacora[0].avance);
                                $("#barraProgreso" + bitacora[0].idKeyResult).css("width", porcentaje + "%");
                                $("#autorizaAvance" + bitacora[0].idKeyResult).hide();
                                $("#avance" + bitacora[0].idKeyResult).html(bitacora[0].avance);
                                $("#small" + bitacora[0].idKeyResult).text(porcentaje.toFixed(2) + "%");
                                $("#marcadorAvance" + bitacora[0].idKeyResult).hide();

                                $.ajax({
                                    url: 'KeyResultController/getObjetivoByIdKr',
                                    data: {
                                        idKeyResult: bitacora[0].idKeyResult,
                                    },
                                    type: 'POST',
                                    success: function (response) {
                                        infoGraf = JSON.parse(response);
                                        $("#gauge" + infoGraf[0].idObjetivo).empty();

                                        g = new JustGage({
                                            id: 'gauge' + infoGraf[0].idObjetivo,
                                            value: infoGraf[0].avance,
                                            min: 0,
                                            max: 100,
                                            titlePosition: "below",
                                            valueFontColor: "#3f4c6b",
                                            pointer: true,
                                            pointerOptions: {
                                                toplength: -15,
                                                bottomlength: 10,
                                                bottomwidth: 12,
                                                color: '#8e8e93',
                                                stroke: '#ffffff',
                                                stroke_width: 3,
                                                stroke_linecap: 'round'
                                            },
                                            relativeGaugeSize: true,

                                        });
                                    }
                                });

                            } else if (response == 0) {
                                alert("Ubo un error");
                                return 0;
                            }
                        }
                    });
                } else if (response == 0) {
                    alert("Ubo un error");
                    return 0;
                }
            }
        });
    });


    $(".enviarChat").click(function () {
        var idObjetivo = $(this).attr("text");
        var fechaHoy = $("#fechaHoy").val();
        var usuarioLogueado = $("#usuarioLogueado").val();
        var nombreLogueado = $("#nombreLogueado").val();
        var men = $("#mensajeChat" + idObjetivo).val();
        var mensaje = '' +
            '<li>' +
            '<div class="col-md-10 mensajeChatDer">' +
            '<label>' + nombreLogueado + '</label>' +
            '<p>' + men + '</p>' +
            '<small class="col-md-12 fechamen text-right">' + fechaHoy + '</small>' +
            '</div>' +
            '</li>';
        $("#chatPlan" + idObjetivo).append(mensaje);
        $("#PlanChat" + idObjetivo).animate({scrollTop: $('#PlanChat' + idObjetivo)[0].scrollHeight}, 1000);
        $("#mensajeChat").val('');

        $.ajax({
            url: 'ChatController/insert',
            dataType: "json",
            data: {
                mensaje: men,
                idUsuario: $("#usuarioLogueado").val(),
                tipo: 'objetivo',
                idTipo: idObjetivo
            },
            type: 'POST',
            success: function (response) {
                $("#mensajeChat" + idObjetivo).val('')
            }
        });

    });

    $(".agregaAnexo").click(function () {
        idAnexo++;
        $("#formAnexo").append('' +
            '<div id="anexo' + idAnexo + '" style="height: 50px">' +
            '	<div class="col-md-8 col-sm-8 col-xs-12">' +
            '		<input type="file" class="form-control anexoFile" name="' + idAnexo + '" id="ane' + idAnexo + '">' +
            '		<span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>' +
            '	</div>' +
            '	<div class="col-md-4 text-center" style="margin-top: 5px;">' +
            '		<button class="btn btn-danger btn-xs elimAnex" value="' + idAnexo + '" >' +
            '			<i class="fa fa-trash"></i>Eliminar' +
            '       </button>' +
            '	</div>' +
            '</div>'
        );
        $(".elimAnex").click(function () {
            var valorFila = $(this).attr("value");
            var archivo = $("#ane" + valorFila).val();
            $("#anexo" + valorFila).remove();
            $.ajax({
                url: 'BitacoraKrController/borrarArchivoTemporal',
                dataType: "json",
                data: {
                    archivo: archivo,
                },
                type: 'POST',
                success: function (response) {
                }
            });

        });
    });


    $("#btnFinalizar").click(function () {
        $("#formAnexo").empty();
    });

    $(document).on('change', '.anexoFile', function (e) {
        var data = new FormData();
        var lugar = $(this).attr("name");

        var inputFileImage = document.getElementById("ane" + lugar);
        var file = inputFileImage.files[0];
        data.append('ane' + lugar, file);

        var url = 'BitacoraKrController/uploadFileTemp/';
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,
            success: function (response) {
                if (response != '') {
                    $("#barraCarga").fadeOut(5000);
                    $("#ane" + lugar).attr("type", "text");
                    $("#ane" + lugar).val(response);
                    $("#ane" + lugar).attr("readonly", true);
                    $("#ane" + lugar).css("padding-left", "45px");
                }

            },
            xhr: function () {
                $("#barraProgresoCarga").css("width", 0 + "%");
                $("#barraCarga").show();
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.onprogress = function (e) {
                    if (e.lengthComputable) {
                        var porcentaje = Math.floor((e.loaded / e.total) * 100);
                        $("#barraProgresoCarga").attr("data-transitiongoal", porcentaje);
                        $("#barraProgresoCarga").css("width", porcentaje + "%");
                        $("#porcentajeProgreso").text(porcentaje + '%');
                    }
                }
                return xhr;
            }
        });
    });


    $("#aceptarCancelacion").click(function () {
        var idBitacora = bitacora[0].idBitacora;
        var motivo = $("#motivo").val();
        $.ajax({
            url: 'BitacoraKrController/rechazar',
            data: {
                idBitacora: idBitacora,
                motivo: motivo,
                userNoAutorizo: 'superadmin'
            },
            type: 'POST',
            success: function (response) {
                $("#autorizaAvance" + bitacora[0].idKeyResult).hide();

            }
        });
    });

    $("#btnAceptarCancelado").click(function () {
        var idBitacora = info[0].idBitacora;
        $.ajax({
            url: 'BitacoraKrController/validaCancelado',
            data: {
                idBitacoraEnvio: idBitacora
            },
            type: 'POST',
            success: function (response) {
                if (response > 0) {
                    $("#modalCancelado").modal('toggle');
                    $("#avanceCancelado" + info[0].idKeyResult).hide();

                } else {
                    alert("Ubo un error");
                }
            }
        });
    });

});
