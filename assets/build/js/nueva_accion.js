$(document).ready(function () {
    $("#objetivos").change(function () {
        var idObejtivo = $(this).val();
        $.ajax({
            url: 'KeyResultController/getByObjetivos',
            data: {
                idObjetivo: idObejtivo
            },
            type: 'POST',
            success: function (response) {
                var dataKr = JSON.parse(response);
                $("#kr").empty();
                if(dataKr.length>0) {
                    $("#kr").append("<option value=0>Seleccionar Key Result</option>");
                    for (var i = 0; i < dataKr.length; i++) {
                        $("#kr").append("<option value='"+dataKr[i].idKeyResult+"'>" + dataKr[i].descripcion + "</option>");
                    }
                }else{
                    $("#kr").append("<option value=0>El objetivo no cuenta con Key Result</option>");
                }
            }
        });

    });
    
    $("#guardar").click(function () {
        if( $("#accion").val()!='' ){
            var accion = $("#accion").val();
        }else {
            $("#msj_accion").html("Debe llenar el cambio de accion");
            $("#msj_accion").css({"color": "red"})
        }
        if( $("#objetivos").val()!=0 ){
            var objetivos = $("#objetivos").val();
        }else{
            $("#msj_objetivos").html("Debe seleccionar un objetivo");
            $("#msj_objetivos").css({"color": "red"})
        }
        if( $("#kr").val()!=0 ){
            var kr = $("#kr").val();
        }else{
            $("#msj_kr").html("Debe seleccionar un key result");
            $("#msj_kr").css({"color": "red"})
        }
        $.ajax({
            url: "AccionesController/insert",
            data:{
                accion: accion,
                idKr: kr
            },
            type: "POST",
            success: function (response) {
                if(response==1){
                    alert("Accion generada correctamene");
                    window.location="lista_acciones";
                }else{
                    alert("Error");
                }
            }

        });
    });
});
