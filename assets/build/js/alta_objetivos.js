$(document).ready(function () {
	banderaRadio=0;

	$(".radio").click(function () {
		dico = $("input[name='optionsRadios']:checked").val();
		if(dico=='Dicotomica') {
			$(".cfo").hide();
			banderaRadio=1;
		}else{
			$(".cfo").show();
			banderaRadio=0;
		}
	});


	objetivoGuardado = 0;

	$("#mv").focus(function () {
		$("#msj_mv").hide();
		$("#mv").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#objetivo").focus(function () {
		$("#msj_objetivo").hide();
		$("#objetivo").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#descripcion").focus(function () {
		$("#msj_descripcion").hide();
		$("#descripcion").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#inicio").focus(function () {
		$("#msj_inicio").hide();
		$("#inicio").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#final").focus(function () {
		$("#msj_final").hide();
		$("#final").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});

	$("#medicioncomienzo").focus(function () {
		$("#msj_comienzo").hide();
		$("#medicioncomienzo").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#medicionfinal").focus(function () {
		$("#msj_tope").hide();
		$("#medicionfinal").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});

	/*$("#orden").focus(function () {
		$("#msj_orden").hide();
		$("#orden").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});*/

	$("#mv").change( function () {
		var idMv = $(this).val();
		$.ajax({
			url: "PlanesController/getAnual",
			data:{
				idmv: idMv
			},
			type: "POST",
			success:function (response) {
				if(response==1){
                    $("#anual").prop('checked', false);
                    $("#grupoCheck").hide();
				}else if(response==0){
                    $("#anual").prop('checked', false);
                    $("#grupoCheck").show();
				}
            }
		});
    });

	$("#descripcionkr").focus(function () {
		$("#msj_descripcionkr").hide();
		$("#descripcionkr").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});

	$("#btnSiguiente").click(function () {
		var avanza = 1;
		var obj = new Object();

		if ($("#mv").val() == '0') {
			$("#msj_mv").html("Debe seleccionar unas misión / Visióp");
			$("#mv").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#mv").val() != '') {
			obj.idmv = $("#mv").val();
		}

		//Validamos el input del objetivo
		if ($("#objetivo").val() == '') {
			$("#msj_objetivo").html("Debe ingresar el objetivo");
			$("#objetivo").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#objetivo").val() != '') {
			obj.objetivo = $("#objetivo").val();
		}

        //Validamos el input del objetivo
        if ($("#anual").is(':checked')) {
        	obj.anual = 1;
        } else  {
            obj.anual = 0;
        }

		//Validamos el input de la descripcion
		if ($("#descripcion").val() == '') {
			$("#msj_descripcion").html("Debe ingresar la descripcion del objetivo");
			$("#descripcion").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#descripcion").val() != '') {
			obj.descripcion = $("#descripcion").val();
		}

		//Validamos el input de la fecha inicial
		if ($("#inicio").val() == '') {
			$("#msj_inicio").html("Debe ingresar la fecha inicial del objetivo");
			$("#inicio").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#inicio").val() != '') {
			obj.finicio = $("#inicio").val();
		}

		//Validamos el input de la fecha final
		if ($("#final").val() == '') {
			$("#msj_final").html("Debe ingresar la fecha final del objetivo");
			$("#final").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#final").val() != '') {
			obj.ffinal = $("#final").val();
		}

		if ($("#final").val() != '' && $("#inicio").val() != '') {
			if ((new Date(obj.finicio).getTime() > new Date(obj.ffinal).getTime())) {
				$("#msj_inicio").html("La fecha inicial debe ser mayor a la final");
				$("#msj_inicio").show();
				$("#inicio").css({"border": "1px solid red", "border-radius": "4px"});
				avanza = 0;
			}
		}

		if (objetivoGuardado == 1)
			var url = 'ObjetivosController/update/' + idObjetivo;
		else
			var url = 'ObjetivosController/insert';


		var dataObjt = JSON.stringify(obj);
		if (avanza == 1) {
			$.ajax({
				url: url,
				dataType: "json",
				data: {
					obj: dataObjt
				},
				type: 'POST',
				success: function (response) {
					if (response != 0) {
						idObjetivo = response;
						objetivoGuardado = 1;
						$("#btnSiguiente").hide();
						$("#step-1").hide();
						$("#btnGuardar").show();
						$("#btnNkr").show();
						$("#btnFinalizar").show();
						$("#btnAtras").show();
						$("#step-2").show();
					} else if (response == 0) {
						return false;
					}
				},
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr() ;
                    xhr.onloadstart = function(e) {
                        $("#fondoLoader").show();
                    };
                    xhr.upload.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(2000);
                    }
                    return xhr ;
                }
			});
		}
	});

	$("#btnAtras").click(function () {
		$("#step-1").show();
		$("#step-2").hide();
		$("#btnSiguiente").show();
		$("#btnFinalizar").hide();
		$("#btnAtras").hide();
		$("#btnGuardar").hide();
		$("#btnNkr").hide();
	});

	$("#btnGuardar").click(function () {
		var finalizar = 1;

		if ($("#descripcionkr").val() == '') {
			$("#msj_descripcionkr").html("Debe ingresar la descripcion de la key result");
			$("#descripcionkr").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#descripcionkr").val() != '') {
			var descripcionkr = $("#descripcionkr").val();
		}


		if (!$("input[name='optionsRadios']:checked").val()) {
			finalizar = 0;
			$("#msj_metrica").html("Debe ingresar la opcion de medicion");
		} else {
			var opcion = $("input[name='optionsRadios']:checked").val();
		}

		if(banderaRadio==0) {
			if ($("#medicioncomienzo").val() == '') {
				$("#msj_comienzo").html("Debe ingresar el inicio de la metrica");
				$("#medicioncomienzo").css({"border": "1px solid red", "border-radius": "4px"});
				finalizar = 0;
			} else if ($("#medicioncomienzo").val() != '') {
				var medicioncomienzo = $("#medicioncomienzo").val();
			}

			if ($("#medicionfinal").val() == '') {
				$("#msj_tope").html("Debe ingresar el fin de la metrica");
				$("#medicionfinal").css({"border": "1px solid red", "border-radius": "4px"});
				finalizar = 0;
			} else if ($("#medicionfinal").val() != '') {
				var medicionfinal = $("#medicionfinal").val();
			}

			if ($("#medicioncomienzo").val() != '' && $("#medicionfinal").val() != '') {
				/*Si hay datos*/

				if (medicioncomienzo > medicionfinal) {
					$("#msj_comienzo").html("El valor inicial de la metrica debe ser mayor al tope");
					$("#msj_comienzo").show();
					$("#medicioncomienzo").css({"border": "1px solid red", "border-radius": "4px"});
					finalizar = 0;
				}
			}

			/*if ($("#orden").val() == '0') {
				$("#msj_orden").html("Debes seleecionar el orden");
				$("#orden").css({"border": "1px solid red", "border-radius": "4px"});
				finalizar = 0;
			} else if ($("#orden").val() != '') {
				var orden = $("#orden").val();
			}*/
			var orden = 'Ascendente';
		}else{
			medicioncomienzo=1;
			medicionfinal=100;
			var orden='s/o';
		}



		if (finalizar == 0) {
			return false;
		} else if (finalizar == 1) {
			$.ajax({
				url: 'KeyResultController/insert',
				data: {
					idObjetivo: idObjetivo,
					descripcion: descripcionkr,
					metrica: opcion,
					orden: orden,
					medicioncomienzo: medicioncomienzo,
					medicionfinal: medicionfinal,
					avance: 0
				},
				type: 'POST',
				success: function (response) {
					if (response == 1) {
						$('#myModal').modal();
					} else if (response == 0) {
						alert("Ubo un error");
					}
				}
			});
		}
	});


	$("#btnNkr").click(function () {
		$('#myModal').modal('hide');
		$(".cfo").show();
		$("#medicioncomienzo").val('');
		$("#medicionfinal").val('');
		$("#descripcionkr").val('');
		$("input[name='optionsRadios']:checked").prop('checked', false);
		//$("#orden").val(0);
	});

	$(".btnFinalizar").click(function () {
		window.location = 'lista_objetivos';
	});

});
