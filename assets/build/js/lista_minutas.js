function showStatus(online) {
    const statusEl = document.querySelector('.onlineState');

    if (online) {
        statusEl.classList.remove('warning');
        statusEl.classList.add('success');
        statusEl.innerText = ` (Online)`;
    } else {
        statusEl.classList.remove('success');
        statusEl.classList.add('warning');
        statusEl.innerText = ` (Ofline)`;
    }
}

window.addEventListener('load', () => {
    // 1st, we set the correct status when the page loads
    navigator.onLine ? showStatus(true) : showStatus(false);

    // now we listen for network status changes
    window.addEventListener('online', () => {
        showStatus(true);
    });

    window.addEventListener('offline', () => {
        showStatus(false);
    });
});


function editaAcuerdo(idAcuerdo){
    window.location = 'edita_acuerdo/' + idAcuerdo;

}




$(document).ready(function () {


    $(".updateAvance").click(function () {
        $("#avance").val('');
        $("#descripcionavance").val('');
        idAcuerdo = $(this).val();
        $.ajax({
            url: 'BitacoraAcuerdoController/validaAprobado',
            data: {
                idAcuerdo: idAcuerdo,
            },
            type: 'POST',
            success: function (response) {
                if (response == 1) {
                    $.ajax({
                        url: 'AcuerdosController/getById',
                        data: {
                            idAcuerdo: idAcuerdo,
                        },
                        type: 'POST',
                        success: function (response) {
                            if (response != 0) {
                                acuerdo = JSON.parse(response);
                                $("#id_de_la_minuta").val(acuerdo[0].idMInuta);
                                console.log(acuerdo);
                                $("#tipometrica").html(acuerdo[0].actividad);
                                $("#actual").html('Último avance : ' + acuerdo[0].avance);
                                $("#rango").html('El rango debe de ser entre (0)  y (100)');
                                $('#myModal').modal();
                            } else if (response == 0) {
                                alert("Ubo un error");
                            }
                        }
                    });
                } else if (response == 0) {
                    $('#aprobadoCancel').modal();
                }
            }
        });
    });

    $("#btnAceptar").click(function () {
        var i = 1;
        var conArchivos = 0;


        $("#formAnexo").find('input:file').each(function () {
            $(this).prop("id", "ane" + i);
            i++;
        });

        var avanza = 1;
        if ($("#avance").val() == '') {
            $("#msj_avance").show();
            $("#msj_avance").html("Debe ingresar el avance");
            $("#avance").css({"border": "1px solid red", "border-radius": "4px"});
            avanza = 0;
        } else if ($("#avance").val() != '') {
            if ((parseInt($("#avance").val()) > parseInt(0)) &&
                (parseInt($("#avance").val()) <= parseInt(100)) &&
                (parseInt($("#avance").val()) > parseInt(acuerdo[0].avance))
            ) {
                var avance = $("#avance").val();
            } else {
                $("#msj_avance").show();
                $("#msj_avance").html("Debe de ser mayor al último avance");
                $("#msj_avance").css({"color": 'red'});
                avanza = 0;
            }
        }
        if ($("#descripcionavance").val() == '') {
            $("#msj_descripcionavance").show();
            $("#msj_descripcionavance").html("Debe ingresar el avance");
            $("#descripcionavance").css({"border": "1px solid red", "border-radius": "4px"});
            avanza = 0;
        } else if ($("#descripcionavance").val() != '') {
            var descripcionavance = $("#descripcionavance").val();
        }

        if (avanza == 1) {
            $.ajax({
                url: 'BitacoraAcuerdoController/insert',
                data: {
                    idAcuerdo: idAcuerdo,
                    descripcion: descripcionavance,
                    ultimoAvance: acuerdo[0].avance,
                    avance: avance,
                    aprobado: 0,
                    user: $("#usuarioLogueado").val(),
                },
                type: 'POST',
                success: function (response) {
                    if (response != 0) {
                        var av = parseFloat(response).toFixed(2);
                        console.log(av);
                        $('#myModal').modal('toggle');
                        $("#marcadorAvance" + idAcuerdo).show();
                        $("#updateAvance" + idAcuerdo).hide();
                        if($("#tipoUsuario").val()=='superadmin' || $("#tipoUsuario").val()=='admin'){
                            $("#small" + idAcuerdo).empty();
                            $("#small" + idAcuerdo).html(parseFloat(avance).toFixed(2)+'%');
                            $("#marcadorAvance" + idAcuerdo).html(parseFloat(av).toFixed(2)+'%');
                            //Edizon
                            var gra = eval("g"+parseInt($("#id_de_la_minuta").val()));
                            gra.refresh(av);
                        }else {
                            $("#marcadorAvance" + idAcuerdo).html(' ( ' + parseFloat(av).toFixed(2) + '% sin autorizar )');
                            $("#marcadorAvance" + idAcuerdo).css({'color': 'red'});
                        }

                        $("#avance").val('');
                        $("#descripcionavance").val('');
                    } else {
                        alert("Ubo un error");
                    }
                }
            });
        }
    });




    $(".eliminaMinuta").click(function () {
        //Mostramos modal de validacion
        $("#leyendaEliminar").text('Esta apunto de eliminar una minuta')
        $('#modalClave').modal();
        idMinuta = $(this).val();
        acuerd = 0;
        mMinut = 1;
    });
    $(".eliminaAcuerdo").click(function () {
        //Mostramos modal de validacion
        $("#leyendaEliminar").text('Esta apunto de eliminar un acuerdo')
        $('#modalClave').modal();
        idAcuerdo = $(this).val();
        acuerd = 1;
        mMinut = 0;
    });

    $("#validaClave").click(function () {
        var clave = $("#contraseña").val();
        if (clave != '') {
            $.ajax({
                url: 'UsuariosController/getUserByClaveUser',
                data: {
                    user: $("#usuarioLogueado").val(),
                    clave: clave
                },
                type: 'POST',
                success: function (response) {
                    if (response == 1) {
                        if (mMinut == 1) {
                            $.ajax({
                                url: 'MinutasController/elimina/' + idMinuta,
                                data: {},
                                type: 'POST',
                                success: function (response) {
                                    if (response != 0) {
                                        $(".listaMinutas" + idMinuta).hide();
                                        $('#modalClave').modal('toggle');
                                    } else if (response == 0) {
                                        alert("Contraseña incorrecta");
                                    }
                                }
                            });
                        } else if (acuerd == 1) {
                            $.ajax({
                                url: 'AcuerdosController/eliminar/' + idAcuerdo,
                                data: {},
                                type: 'POST',
                                success: function (response) {
                                    $("#listaAc"+idAcuerdo).hide();
                                    $('#modalClave').modal('toggle');
                                    /*var arrayObj = JSON.parse(response);
                                    console.log(arrayObj);
                                    if (response != 0) {
                                        $(".listaKr" + krElimina).remove();

                                        $("#gauge" + arrayObj.idObjetivo).empty();

                                        g = new JustGage({
                                            id: 'gauge' + arrayObj.idObjetivo,
                                            value: arrayObj.porcentajeObjetivo.toFixed(2),
                                            min: 0,
                                            max: 100,
                                            titlePosition: "below",
                                            valueFontColor: "#3f4c6b",
                                            pointer: true,
                                            pointerOptions: {
                                                toplength: -15,
                                                bottomlength: 10,
                                                bottomwidth: 12,
                                                color: '#8e8e93',
                                                stroke: '#ffffff',
                                                stroke_width: 3,
                                                stroke_linecap: 'round'
                                            },
                                            relativeGaugeSize: true,

                                        });


                                        $('#modalClave').modal('toggle');
                                    } else if (response == 0) {
                                        alert("Ubo un error");
                                    }*/
                                }
                            });
                        }

                    } else if (response == 0) {
                        alert("Contraseña incorrecta");
                    }
                }
            });
        }
    });




    $(".bitacoraLista").click(function () {
        var i = 0;
        $.ajax({
            url: 'BitacoraAcuerdoController/getByIdAcuerdo',
            data: {
                idAcuerdo: $(this).val(),
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $("#tablaBitacora tbody tr").remove();
                    var dataResponse = JSON.parse(response);
                    console.log(dataResponse);
                    $("#tIndicador").html(dataResponse[0].tituloAcuerdo);
                    for(i=0; i< dataResponse.length; i++ ) {
                        $("#tablaBitacora tbody").append(
                                "<tr>" +
                                "<td>" +
                                dataResponse[i].fecha +
                                "</td>" +
                                "<td>" +
                                dataResponse[i].descripcion +
                                "</td>" +
                                "<td>" +
                                dataResponse[i].ultimoAvance +
                                "</td>" +
                                "<td>" +
                                dataResponse[i].avance +
                                "</td>" +
                                "<td>" +
                                dataResponse[i].aprobado +
                                "</td>" +
                                "<td>" +
                                dataResponse[i].user +
                                "</td>" +
                                "<td>" +
                                dataResponse[i].motivo +
                                "</td>" +
                                "</tr>"
                        );
                    }
                    $("#modalBitacora").modal();
                } else if (response == 0) {
                    $("#tablaBitacora tbody tr").remove();
                    $("#indicadorT").html("Sin bitácora");
                    $("#tIndicador").html("");
                    $("#modalBitacora").modal();

                }
            }
        });
    });


    $(".verAcuerdos").click(function () {
        var i = 0;
        $.ajax({
            url: 'MinutasController/getPdfByIdMinuta',
            data: {
                idMinuta: $(this).val(),
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $("#tablaBitacora tbody tr").remove();
                    var dataResponse = JSON.parse(response);
                    console.log(dataResponse.length);
                    if(dataResponse.length>0) {
                        $("#tIndicador").html(dataResponse[0].tituloAcuerdo);
                        for (i = 0; i < dataResponse.length; i++) {
                            $("#tablaBitacora tbody").append(
                                "<tr>" +
                                "<td style='font-size: 15px;'>" +
                                "<a download href='pdfsminutas/" + dataResponse[i].pdf + "'><i style='color: red;' class='fa fa-file-pdf-o fa-2x'></i> " + dataResponse[i].pdf + "</a>" +
                                "</td>" +
                                "</tr>"
                            );
                        }
                        $("#modalAcuerdos").modal();
                    }else{
                        $("#tablaBitacora tbody tr").remove();
                        $("#indicadorT").html("Sin bitácora");
                        $("#tIndicador").html("");
                        $("#modalAcuerdos").modal();
                    }
                }
            }
        });
    });

});