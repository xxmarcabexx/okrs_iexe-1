$(document).ready(function () {
	banderaRadio = null;
	$(".radio").click(function () {
		banderaRadio = 0;
		dico = $("input[name='optionsRadios']:checked").val();
		if (dico == 'Dicotomica') {
			$(".cfo").hide();
			banderaRadio = 1;
		}else if(dico =='Porcentaje'){
			$("#medicioncomienzo").val('1');
			$("#medicionfinal").val('100');
			$("#medicioncomienzo").attr('disabled',true);
			$("#medicionfinal").attr('disabled',true);
		} else {
			$("#medicioncomienzo").attr('disabled',false);
			$("#medicionfinal").attr('disabled',false);
			$("#medicioncomienzo").val('');
			$("#medicionfinal").val('');
			$(".cfo").show();
			banderaRadio = 0;
		}
	});


	$("#medicioncomienzo").focus(function () {
		$("#msj_comienzo").hide();
		$("#medicioncomienzo").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#medicionfinal").focus(function () {
		$("#msj_tope").hide();
		$("#medicionfinal").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});

	/*$("#orden").focus(function () {
		$("#msj_orden").hide();
		$("#orden").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});*/

	$("#descripcionkr").focus(function () {
		$("#msj_descripcionkr").hide();
		$("#descripcionkr").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});


	/*$("#btnGuardar").click(function () {
		var finalizar = 1;

		if ($("#descripcionkr").val() == '') {
			$("#msj_descripcionkr").html("Debe ingresar la descripcion de la key result");
			$("#descripcionkr").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#descripcionkr").val() != '') {
			var descripcionkr = $("#descripcionkr").val();
		}


		if (!$("input[name='optionsRadios']:checked").val()) {
			finalizar = 0;
			$("#msj_metrica").html("Debe ingresar la opcion de medicion");
		} else {
			var opcion = $("input[name='optionsRadios']:checked").val();
		}
		if (banderaRadio == 0) {
			if ($("#medicioncomienzo").val() == '') {
				$("#msj_comienzo").html("Debe ingresar el inicio de la metrica");
				$("#medicioncomienzo").css({"border": "1px solid red", "border-radius": "4px"});
				finalizar = 0;
			} else if ($("#medicioncomienzo").val() != '') {
				var medicioncomienzo = $("#medicioncomienzo").val();
			}

			if ($("#medicionfinal").val() == '') {
				$("#msj_tope").html("Debe ingresar el fin de la metrica");
				$("#medicionfinal").css({"border": "1px solid red", "border-radius": "4px"});
				finalizar = 0;
			} else if ($("#medicionfinal").val() != '') {
				var medicionfinal = $("#medicionfinal").val();
			}

			if ($("#medicioncomienzo").val() != '' && $("#medicionfinal").val() != '') {
				console.log(medicioncomienzo + ">" + medicionfinal);
				if (parseFloat(medicioncomienzo) > parseFloat(medicionfinal)) {
					$("#msj_comienzo").html("El valor inicial de la metrica debe ser mayor al tope");
					$("#msj_comienzo").show();
					$("#medicioncomienzo").css({"border": "1px solid red", "border-radius": "4px"});
					finalizar = 0;
				}
			}
			var orden = "Ascendente";
		} else {
			medicioncomienzo = 1;
			medicionfinal = 100;
			var orden = 's/o';
		}
		if (finalizar == 0) {
			return false;
		} else if (finalizar == 1) {
			$.ajax({
				url: '../KeyResultController/insert',
				data: {
					idObjetivo: $("#idObjetivo").val(),
					descripcion: descripcionkr,
					metrica: opcion,
					orden: orden,
					medicioncomienzo: medicioncomienzo,
					medicionfinal: medicionfinal,
					avance: 0
				},
				type: 'POST',
				success: function (response) {
					if (response == 1) {
						$('#myModal').modal();
					} else if (response == 0) {
						alert("Ubo un error");
					}
				}
			});
		}
	});*/


	/*$("#btnNkr").click(function () {
		$('#myModal').modal('hide');
		$("#medicioncomienzo").val('');
		$("#medicionfinal").val('');
		$("#descripcionkr").val('');
		$("input[name='optionsRadios']:checked").prop('checked', false);
		//$("#orden").val(0);
	});*/


	$("#btnGuardar").click(function () {
		var columna = 1;
		var step = 1;
		var compara = 11;
		var krs = new Array();
		var texto = 1;
		avanza=1;
		i=0;
		$("td").each(function () {
			if (columna>6)
				if(columna==compara) {
					compara++;
					step++;
					if(step==3){
						i++;
						compara+=4;
						step=1;
					}
				}
				else {
					valor = $(this).text();
					switch (texto) {
						case 1:
							krs[i] = new Object();
							krs[i].nombre = valor;
							break;
						case 2:
							krs[i].medicion = valor;
							break;

						case 3:
							krs[i].vinicial = valor;
							break;
						case 4:
							krs[i].vfinal = valor;
							break;
					}
					if(texto>=4){
						texto=1;
					}else {
						texto++;
					}

				}
			columna++;
		});
		if(krs.length<=0){
			$("#mensajeKr").show();
			$("#mensajeKr").fadeOut(6000);
		}else{
			$.ajax({
				url: '../KeyResultController/InsertMultiple/'+$("#idObjetivo").val(),
				dataType: "json",
				data: {
					obj: krs
				},
				type: 'POST',
				success: function (response) {
					if (response != 0) {
                        alert("Los registros han sido guardados correctamente");
                        window.location = '../lista_objetivos';
					} else if (response == 0) {
						alert("Error");
					}
				},
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr() ;
                    xhr.onloadstart = function(e) {
                        $("#fondoLoader").show();
                    };
                    xhr.upload.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(2000);
                    }
                    return xhr ;
                }
			});
		}
	});


	$(".btnFinalizar").click(function () {
		var finalizar = 1;

		if ($("#descripcionkr").val() == '') {
			$("#msj_descripcionkr").html("Debe ingresar la descripcion de la key result");
			$("#descripcionkr").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#descripcionkr").val() != '') {
			var descripcionkr = $("#descripcionkr").val();
		}


		if (!$("input[name='optionsRadios']:checked").val()) {
			finalizar = 0;
			$("#msj_metrica").html("Debe ingresar la opcion de medicion");
		} else {
			var opcion = $("input[name='optionsRadios']:checked").val();
		}
		if (banderaRadio == 0) {
			if ($("#medicioncomienzo").val() == '') {
				$("#msj_comienzo").html("Debe ingresar el inicio de la metrica");
				$("#medicioncomienzo").css({"border": "1px solid red", "border-radius": "4px"});
				finalizar = 0;
			} else if ($("#medicioncomienzo").val() != '') {
				var medicioncomienzo = $("#medicioncomienzo").val();
			}

			if ($("#medicionfinal").val() == '') {
				$("#msj_tope").html("Debe ingresar el fin de la metrica");
				$("#medicionfinal").css({"border": "1px solid red", "border-radius": "4px"});
				finalizar = 0;
			} else if ($("#medicionfinal").val() != '') {
				var medicionfinal = $("#medicionfinal").val();
			}

			if ($("#medicioncomienzo").val() != '' && $("#medicionfinal").val() != '') {
				/*Si hay datos*/
				console.log(medicioncomienzo + ">" + medicionfinal);
				if (parseFloat(medicioncomienzo) > parseFloat(medicionfinal)) {
					$("#msj_comienzo").html("El valor inicial de la metrica debe ser mayor al tope");
					$("#msj_comienzo").show();
					$("#medicioncomienzo").css({"border": "1px solid red", "border-radius": "4px"});
					finalizar = 0;
				}
			}
			var orden = "Ascendente";
		} else {
			medicioncomienzo = 1;
			medicionfinal = 100;
			var orden = 's/o';
		}
		if (finalizar == 0) {
			return false;
		} else if (finalizar == 1) {
			$("#tablaAdd").show();
			fila = "" +
				"<tr class='fila'>" +
				"<td>" + descripcionkr + "</td>" +
				"<td>" + opcion + "</td>" +
				"<td>" + medicioncomienzo + "</td>" +
				"<td>" + medicionfinal + "</td>" +
				"<td><button class='btn btn-success bed btn-xs'><i class='fa fa-pencil'></i> Editar</button></td>" +
				"<td><button class='btn btn-danger bel btn-xs'><i class='fa fa-pencil'></i> Eliminar</button></td>" +
				"</tr>";
			$("#tablaAdd table").append(fila);
			$("#medicioncomienzo").val('');
			$("#medicionfinal").val('');
			$("#descripcionkr").val('');
			$("input[name='optionsRadios']:checked").prop('checked', false);
		}
	});

	$(document).on('click', '.bel', function (e) {
		$(this).parents("tr").remove();

	});

	$(document).on('click', '.bed', function (e) {
		$(this).parents("tr").remove();
		var valores = new Array();
		$(this).parents("tr").find("td").each(function () {
			valores.push($(this).html());
		});
		$("#medicioncomienzo").val(valores[2]);
		$("#medicionfinal").val(valores[3]);
		$("#descripcionkr").val(valores[0]);
		console.log(valores[1]);
		switch (valores[1]) {
			case 'Porcentaje':
				$("#metrica1").prop("checked", true);
				break;
			case 'Numero':
				$("#metrica2").prop("checked", true);
				break;
			case 'Financiero':
				$("#metrica3").prop("checked", true);
				break;
			case 'Dicotomica':
				$("#metrica4").prop("checked", true);
				break;
		}


	});

});
