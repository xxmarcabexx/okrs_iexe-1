<?php require_once 'complementos/head.php' ?>


<script src="<?php echo base_url(); ?>assets/build/js/nueva_minuta.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/nueva_minuta.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/build/css/wickedpicker.css" rel="stylesheet">




<link href="<?php echo base_url(); ?>assets/build/datepicker/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/build/datepicker/js/datepicker.min.js"></script>
<!-- Include English language -->
<script src="<?php echo base_url(); ?>assets/build/datepicker/js/i18n/datepicker.es.js"></script>





</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Módulo de acuerdos</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Registro de acuerdos</h2>
								<div class="clearfix"></div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-horizontal form-label-left">
										<div class="form-group" id="grupoKr">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Plan<span class="required"></span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<select class="form-control has-feedback-left" id="plan" name="plan">
													<option value=0>Selecciona que Key Result puede actualizar el capturista
													</option>
													<?php foreach ($planes as $plan) { ?>
														<option name="<?php echo $plan->idMv?>" value="<?php echo (isset($plan->codigo))?"$plan->codigo":"0";?>"><?php echo $plan->mv;?></option>
													<?php } ?>
												</select>
												<span class="fa fa-sort form-control-feedback left" aria-hidden="true"></span>
												<small id="msj_kr"></small>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												   for="first-name">Codigo de acuerdo<span class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<input type="text" id="codigo"
													   class="form-control col-md-7 col-xs-12 has-feedback-left" readonly>
												<span class="fa fa-user form-control-feedback left"
													  aria-hidden="true"></span>
												<small id="msj_usuario"></small>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Objetivo<span
													class="required"></span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<textarea class="form-control" id="objetivo"></textarea>
												<small id="msj_clave"></small>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Asunto a tratar<span
													class="required"></span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<textarea class="form-control" id="asunto"></textarea>
												<small id="msj_clave_valida"></small>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-horizontal form-label-left">
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												   for="first-name">Fecha<span class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<input type="text" id="fecha" data-language='es'
													   class="datepicker-here form-control col-md-7 col-xs-12 has-feedback-left">
												<span class="fa fa-user form-control-feedback left"
													  aria-hidden="true"></span>
												<small id="msj_usuario"></small>
											</div>
                                        </div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												   for="first-name">Hora<span class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<input type="text" id="hora" name="timepicker"
													   class="form-control col-md-7 col-xs-12 has-feedback-left timepicker">
												<span class="fa fa-user form-control-feedback left"
													  aria-hidden="true"></span>
												<small id="msj_usuario"></small>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												   for="first-name">Lugar<span class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<input type="text" id="lugar"
													   class="form-control col-md-7 col-xs-12 has-feedback-left">
												<span class="fa fa-user form-control-feedback left"
													  aria-hidden="true"></span>
												<small id="msj_usuario"></small>
											</div>
										</div>
									</div>
								</div>
							</div>

                            <div class="row" style="margin-bottom: 15px; margin-top: 15px;">
                                <div class="col-md-4">
                                    <button class="btn btn-success btn-sm" id="agregarAdjuntos"><i class="fa fa-plus-circle"></i>Adjuntar archivo</button>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <small id="mensajeAexos" style="color: red;"></small>
                                </div>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="project_progress" id="barraCarga" style="display: none;">
                                        <small id="porcentajeProgreso">
                                        </small>
                                        <div class="progress progress_sm">
                                            <small id="porcentajeProgreso" style="color: red;"></small>
                                            <div class="progress-bar bg-green" id="barraProgresoCarga" role="progressbar"
                                                 data-transitiongoal="20" aria-valuenow="56" style="width: 57%;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="row" id="formAnexo"></div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 15px; margin-top: 15px;">
                                <div class="col-md-4">
                                    <button class="btn btn-success btn-sm" id="agregarAcuerdos"><i class="fa fa-plus-circle"></i>Agregar acuerdo</button>
                                </div>
                            </div>

                            <div class="col-md-10 col-md-offset-1">
                                <div class="row" id="formAcuerdos">
                                </div>
                            </div>


                            <div class="row" style=" margin-top: 35px;">
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-success btn-sm" id="guardar"><i class="fa fa-save"></i> Guardar</button>
                                </div>
                                <div class="col-md-6 text-left">
                                    <button class="btn btn-danger btn-sm" id="regresar"><i class="fa fa-save"></i> Regresar</button>
                                </div>
                            </div>

						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body text-center">
						<p>Key Result agregado correctamente.</p>
						<p>¿Deseas agregar otro?</p>
					</div>
					<div class="row">
						<div class="col-md-6 text-right">
							<button data-dismiss="modal" class="btn btn-primary" id="btnNkr">Nueva Kr</button>
						</div>
						<div class="col-md-6 text-left">
							<button class="btn btn-danger btnFinalizar">Finalizar</button>
						</div>
					</div>
				</div>

			</div>
		</div>
		<?php require_once 'complementos/footer.php' ?>
