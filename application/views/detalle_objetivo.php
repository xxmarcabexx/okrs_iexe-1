<?php require_once 'complementos/head.php' ?>
<?php header('Access-Control-Allow-Origin: *'); ?>


<script src="<?php echo base_url(); ?>assets/build/js/detalle_objetivo.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/detalle_objetivos.css" rel="stylesheet">

<style>
	.pf{
		padding: 10px 15px;
		background: #f5f5f5;
		border-top: 1px solid #ddd;
		border-bottom: 1px solid #00AEAA;
		border-left: 1px solid #00AEAA;
		border-right: 1px solid #00AEAA;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;
	}
	.panel{
		margin-bottom: 0 !important;
	}

	/*****************Chat******************/
	.panel-primary {
		border-color: #00AEAA !important;
	}
	.panel-primary>.panel-heading {
		color: #fff;
		background-color: #00AEAA;
		border-color: #00AEAA;
	}
	.enviarChat{
		background: #00AEAA;
	}

	.PlanChat{
		height: 350px!important;
		background-image: url('assets/build/images/chat.jpg');
		overflow: scroll;
	}
	.mensajeChatDer, .mensajeChatIzq{
		margin-right: 0px !important;
		background: white !important;
		padding: 5px 10px !important;
		margin-bottom: 10px !important;
		border-radius: 7px !important;
	}
	.mensajeChatDer{
		float: right !important;
	}
	.mensajeChatDer label{
		color: orange;
	}
	.mensajeChatIzq label{
		color: crimson;
	}
	ul{
		list-style-type: none;

	}


	/***************************************************/

	.tituloPlanes{
		cursor: pointer !important;
		background: #00AEAA !important;
		border-radius: 4px !important;
		color: white !important;
		padding: 10px 0px 10px 0px !important;
	}



	.tituloPlanes h2{
		color: white !important;
	}

</style>

</head>

<!--<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<?php //require_once 'complementos/menu.php' ?>

		<?php //require_once 'complementos/topnavigation.php' ?>


		<div class="right_col" role="main">-->
<input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
	   style="display: none;">
<input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>" style="display: none;">
<input id="fechaHoy" value="<?php echo date('Y-m-d H:i');?>" style="display: none;">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<img src="<?php echo base_url(); ?>assets/build/images/mo.png" class="img-responsive">
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<div class="clearfix"></div>
							</div>
                            <h2 style="font-size: 24px;">Objetivo Anual</h2>
							<div class="x_content">
								<!-- start accordion -->
								<div class="tituloObjetivos row accordion-toggle" data-toggle="collapse" href="#">
									<div class="col-md-4 col-sm-4 col-xs-12">
										<div class="col-md-6">
											<h2><?php echo $objetivo[0]->objetivo ?></h2>
										</div>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<div class="project_progress">
											<small><?php echo $objetivo[0]->avance ?>% Avance</small>
											<div class="progress progress_sm">
												<div class="progress-bar bg-blue" role="progressbar"
													 data-transitiongoal="3" aria-valuenow="56"
													 style="width: <?php echo $objetivo[0]->avance ?>%;"></div>
											</div>
										</div>
									</div>

								</div>
								<div id="">
									<div class="col-md-12 subtitulos">
										<div class="col-md-9">
											<div class="col-md-12">
												<h4>Descripción</h4>
												<p><?php echo $objetivo[0]->descripcion ?></p>
											</div>
											<div class="col-md-12" style="margin-top: 35px;">
												<?php
												if (count($keyresult) > 0)
													foreach ($keyresult as $kr) {
														?>
														<div class="col-md-4 col-md-offset-1 col-sm-4 col-xs-12 bg-white text-center graficas krr" style="cursor: pointer; margin-top: 25px;" title="<?php echo $kr->idKeyResult; ?>">
															<div  id='<?php echo "gauge" . $kr->idKeyResult; ?>'
																 class="2000x1600px"></div>
															<h5 style="font-weight: normal; font-size: 12px;"><?php echo (strlen($kr->descripcion)>90) ? substr($kr->descripcion, 0, 90)."..." : $kr->descripcion; ?></h5>
														</div>
													<?php } ?>
											</div>
										</div>
										<input value="<?php echo $objetivo[0]->idObjetivo;?>" style="display: none;" id="idObjetivo">
										<div class="col-md-3 col-xs-12" style="margin-top:45px">
											<div class="panel panel-primary">
												<div class="panel-heading text-center">Buzón <i class="fa fa-envelope" aria-hidden="true"></i></div>
												<div class="panel-body PlanChat" id="PlanChat<?php echo $objetivo[0]->idObjetivo; ?>">
													<ul id="chatPlan<?php echo $objetivo[0]->idObjetivo; ?>">
														<?php foreach ($chat as $ch) { ?>
															<li>
																<div class="col-md-12 mensajeChatDer" style="width: 120% !important;">
																	<label><?php echo $ch->nombre; ?></label>
																	<p><?php echo $ch->mensaje; ?></p>
																	<small
																		class="col-md-12 fechamen text-right"><?php echo $ch->fechahora; ?></small>
																</div>
															</li>
														<?php } ?>
													</ul>
												</div>
											</div>
											<div class="pf">
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
														<input type="text" class="form-control" id="mensajeChat<?php echo $objetivo[0]->idObjetivo; ?>" placeholder="Escribir mensaje">
													</div>
													<br><br>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
														<button class="btn btn-primary enviarChat" text="<?php echo $objetivo[0]->idObjetivo; ?>">
															Enviar
														</button>
													</div>
												</div>
											</div>
										</div>


									</div>



									<!--Informacion de la ficha tecnica-->
									<!--<div class="clearfix"></div>
									<div class="col-md-12 subtitulos text-center">
										<h3>OKR's</h3>
									</div>
									<div>
										<table class="table table-striped table-condensed">
											<thead>
											<tr>
												<th>Descripción</th>
												<th>Metrica</th>
												<th>Progreso</th>
												<th>Orden</th>
												<th>Inicio</th>
												<th>Final</th>
											</tr>
											</thead>
											<tbody>
											<?php
											if (count($keyresult) > 0)
												foreach ($keyresult as $kr) {
													?>
													<tr>
														<td><?php echo $kr->descripcion; ?></td>
														<td><?php echo $kr->metrica; ?></td>
														<td>
															<div class="project_progress">
																<small><?php echo $kr->avance; ?>% Complete</small>
																<div class="progress progress_sm">
																	<div class="progress-bar bg-green"
																		 role="progressbar"
																		 data-transitiongoal="<?php echo $kr->avance; ?>"
																		 aria-valuenow="56" style="width: 57%;"></div>
																</div>
															</div>
														</td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
												<?php } ?>
											</tbody>
											<tfoot>
											<tr>
												<th>Descripción</th>
												<th>Metrica</th>
												<th>Progreso</th>
												<th>Orden</th>
												<th>Inicio</th>
												<th>Final</th>
												<th></th>
											</tr>
											</tfoot>
										</table>
									</div>-->
								</div>
								<!-- end of accordion -->
							</div>

						</div>
					</div>
				</div>
			</div>
		<!--</div>
		<?php //require_once 'complementos/footer.php' ?>-->

		<script>
			$(document).ready(function () {
				<?php if (count($keyresult) > 0)
				foreach ($keyresult as $kr) { ?>
				var g = new JustGage({
					id: "<?php echo 'gauge' . $kr->idKeyResult?>",
					value: <?php echo $kr->avance?>,
					min: <?php echo $kr->medicioncomienzo?>,
					max: <?php echo $kr->medicionfinal?>,
					titlePosition: "below",
					valueFontColor: "#3f4c6b",
					pointer: true,
					pointerOptions: {
						toplength: -15,
						bottomlength: 10,
						bottomwidth: 12,
						color: '#8e8e93',
						stroke: '#ffffff',
						stroke_width: 3,
						stroke_linecap: 'round'
					},
					relativeGaugeSize: true,

				});
				<?php } ?>


				$(".krr").click(function () {
					$.ajax({
						type: "POST",
						url: 'detalle_keyresult/'+$(this).attr('title'),
						data:{},
						success: function(datos){
							$('#carga').empty();
							$('#carga').html(datos);
						},
                        xhr: function(){
                            var xhr = $.ajaxSettings.xhr() ;
                            xhr.onloadstart = function(e) {
                                $("#fondoLoader").show();
                                console.log("Esta cargando");
                            };
                            xhr.onloadend = function (e) {
                                $("#fondoLoader").fadeOut(500);
                                console.log("Termino de cargar");
                            }
                            return xhr ;
                        }
					});
				});

			});
		</script>
