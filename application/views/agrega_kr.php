<?php require_once 'complementos/head.php' ?>

<script src="<?php echo base_url();?>assets/build/js/agrega_kr.js"></script>
<link href="<?php echo base_url();?>assets/build/css/agrega_kr.css" rel="stylesheet">

</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Módulo Key Result</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Objetivo: <?php echo $nObjetivo; ?>
								</h2><input value="<?php echo $idObjetivo;?>" id="idObjetivo" style="display: none;">
								<div class="clearfix"></div>
							</div>

							<div class="form-horizontal form-label-left" id="step-1">
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Key Result
										<span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<textarea class="form-control col-md-7 col-xs-12 has-feedback-left" id="descripcionkr"></textarea>
										<small id="msj_descripcionkr"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Seleccionar Métrica<span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12" id="selectMetrica">
										<div class="radio">
											<label>
												<input type="radio" value="Porcentaje" id="metrica1"
													   name="optionsRadios">Porcentaje
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" value="Numérico" id="metrica2" name="optionsRadios">Numérico
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" value="Financiero" id="metrica3"
													   name="optionsRadios">Financiero
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" value="Dicotomica" id="metrica4"
													   name="optionsRadios">Dicotomica
											</label>
										</div>
										<small id="msj_metrica"></small>
									</div>
								</div>

								<div class="form-group cfo">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										   for="first-name">Comienzo<span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="number" min=0 step="0.5" id="medicioncomienzo"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-flag-o form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_comienzo"></small>
									</div>
								</div>
								<div class="form-group cfo">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Final<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="number" id="medicionfinal" step="0.5"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-flag-checkered form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_tope"></small>
									</div>
								</div>
								<!--<div class="form-group cfo">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Orden<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control has-feedback-left" id="orden">
											<option value=0>Selecciona el orden de la métrica</option>
											<option value="Ascendente">Ascendente</option>
											<option value="Descendente">Descendente</option>
										</select>
										<span class="fa fa-sort form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_orden"></small>
									</div>
								</div>-->


								<div class="form-group">
									<div class="col-md-6 text-right">
										<button class="btn btn-success btn-xs" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
									</div>
									<div class="col-md-6 text-left">
										<button class="btn btn-warning btnFinalizar btn-xs"><i class="fa fa-plus-circle"></i> Agregar</button>
									</div>
								</div>
								<div class="col-md-offset-3 col-md-6 text-center">
									<h6 style="display: none; color: red;" id="mensajeKr">Se tiene que agregar al menos una Key Result, presionando el botón de agregar</h6>
								</div>
								<div class="col-md-offset-3 col-md-6" style="margin-top: 20px; display: none;" id="tablaAdd">
									<table class="table table-responsive text-center table-bordered table-striped">
										<thead>
											<td>Key Result</td>
											<td>Medición</td>
											<td>Valor inicial</td>
											<td>Valor final</td>
											<td></td>
											<td></td>
										</thead>
									</table>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>



		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body text-center">
						<p>Key Result agregado correctamente.</p>
						<p>¿Deseas agregar otro?</p>
					</div>
					<div class="row">
						<div class="col-md-6 text-right">
							<button data-dismiss="modal" class="btn btn-primary" id="btnNkr">Nueva Kr</button>
						</div>
						<div class="col-md-6 text-left">
							<button class="btn btn-danger btnFinalizar">Finalizar</button>
						</div>
					</div>
				</div>

			</div>
		</div>

		<?php require_once 'complementos/footer.php' ?>
