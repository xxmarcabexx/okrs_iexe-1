<?php require_once 'complementos/head.php' ?>
<?php header('Access-Control-Allow-Origin: *'); ?>

<script src="<?php echo base_url(); ?>assets/build/js/detalle_kr.js"></script>

<link href="<?php echo base_url(); ?>assets/build/css/detalle_kr.css" rel="stylesheet">
<style>
	.pf{
		padding: 10px 15px;
		background: #f5f5f5;
		border-top: 1px solid #ddd;
		border-bottom: 1px solid #00AEAA;
		border-left: 1px solid #00AEAA;
		border-right: 1px solid #00AEAA;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;
	}
	.panel{
		margin-bottom: 0 !important;
	}

	/*****************Chat******************/
	.panel-primary {
		border-color: #00AEAA !important;
	}
	.panel-primary>.panel-heading {
		color: #fff;
		background-color: #00AEAA;
		border-color: #00AEAA;
	}
	.enviarChat{
		background: #00AEAA;
	}

	.PlanChat{
		height: 350px!important;
		background-image: url('assets/build/images/chat.jpg');
		overflow: scroll;
	}
	.mensajeChatDer, .mensajeChatIzq{
		margin-right: 0px !important;
		background: white !important;
		padding: 5px 10px !important;
		margin-bottom: 10px !important;
		border-radius: 7px !important;
	}
	.mensajeChatDer{
		float: right !important;
	}
	.mensajeChatDer label{
		color: orange;
	}
	.mensajeChatIzq label{
		color: crimson;
	}
	ul{
		list-style-type: none;

	}


	/***************************************************/

	.tituloPlanes{
		cursor: pointer !important;
		background: #00AEAA !important;
		border-radius: 4px !important;
		color: white !important;
		padding: 10px 0px 10px 0px !important;
	}



	.tituloPlanes h2{
		color: white !important;
	}

</style>
</head>

<!--<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<?php //require_once 'complementos/menu.php' ?>


		<?php //require_once 'complementos/topnavigation.php' ?>
		<div class="right_col" role="main">-->
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Módulo Key Result</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2><i class="fa fa-align-left"></i> Detalle Key Result</h2>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<!-- start accordion -->
								<div class="tituloObjetivos row accordion-toggle" data-toggle="collapse" href="#">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<h2><?php echo $keyresult[0]->descripcion; ?></h2>
									</div>
								</div>
								<input id="idKr" value="<?php echo $keyresult[0]->idKeyResult;?>" style="display: none;">
								<div id="key_avnce">
									<div class="col-md-offset-2 col-md-8">
										<div class="col-md-6 col-md-offset-3 text-center">
											<?php
											if (count($keyresult) > 0)
												foreach ($keyresult as $kr) {
													?>
													<div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 text-center graficas">
														<div id='<?php echo "gauge" . $kr->idKeyResult; ?>'
															 class="2000x1600px"></div>
														<h5><?php echo $kr->descripcion;?></h5>
													</div>


												<?php } ?>
										</div>
										<div class="col-md-12" style="margin-top: 50px !important;">
											<table class="table table-striped table-condensed">
												<thead>
												<tr>
													<th>Fecha</th>
													<th>Avance</th>
													<th>Descripción</th>
													<th>Status</th>
												</tr>
												</thead>
												<tbody>
												<?php
												if (count($bitacoraKr) > 0)
													foreach ($bitacoraKr as $bitacora) {
														?>
														<tr>
															<td><?php echo $bitacora->fecha; ?></td>
															<td><?php echo $bitacora->avance; ?></td>
															<td><?php echo $bitacora->descripcion; ?></td>
															<td><?php echo ($bitacora->aprobado == 1)? "<p>Aprobado</p>": "<p>Aun no aprobado</p>" ?></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
									<!--<div class="col-md-4">
										<div class="panel panel-primary">
											<div class="panel-heading text-center">Buzón <i class="fa fa-envelope" aria-hidden="true"></i></div>
											<div class="panel-body" id="PlanChat">
												<?php foreach ($chat as $ch){ ?>
													<ul id="chatPlan">
														<li>
															<div class="col-md-10 mensajeChatDer">
																<label><?php echo $ch->idUsuario;?></label>
																<p><?php echo $ch->mensaje;?></p>
															</div>
														</li>
													</ul>
													<!--<div class="col-md-10 mensajeChatIzq">
                                                        <label>Nombre de quien envia</label>
                                                        <p>Mensaje</p>
                                                    </div>  ioioioioi
												<?php } ?>
											</div>
										</div>

										<div class="pf">
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
													<input type="text" class="form-control" placeholder="Escribir mensaje">
												</div>
												<br><br>
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
													<button class="btn btn-primary enviarChat">
														Enviar
													</button>
												</div>
											</div>
										</div>
									</div>-->

								</div>
								<!-- end of accordion -->
							</div>

						</div>
					</div>
				</div>
			</div>
		<!--</div>-->
		<?php //require_once 'complementos/footer.php' ?>

		<script>
			$(document).ready(function () {
				<?php if (count($keyresult) > 0)
				foreach ($keyresult as $kr) { ?>
				var g = new JustGage({
					id: "<?php echo 'gauge' . $kr->idKeyResult?>",
					value: <?php echo $kr->avance?>,
					min: <?php echo $kr->medicioncomienzo?>,
					max: <?php echo $kr->medicionfinal?>,
					titlePosition: "below",
					valueFontColor: "#3f4c6b",
					pointer: true,
					pointerOptions: {
						toplength: -15,
						bottomlength: 10,
						bottomwidth: 12,
						color: '#8e8e93',
						stroke: '#ffffff',
						stroke_width: 3,
						stroke_linecap: 'round'
					},
					relativeGaugeSize: true,

				});
				<?php } ?>

			});
		</script>
-
