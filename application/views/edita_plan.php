<?php require_once 'complementos/head.php'?>

<script src="<?php echo base_url();?>assets/build/js/edita_plan.js"></script>
<link href="<?php echo base_url();?>assets/build/css/edita_plan.css" rel="stylesheet">


<link href="assets/build/datepicker/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="assets/build/datepicker/js/datepicker.min.js"></script>
<!-- Include English language -->
<script src="assets/build/datepicker/js/i18n/datepicker.es.js"></script>


</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php'?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php'?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Modulo Plan</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Creacion de Plan<small></small></h2>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div id="wizard" class="form_wizard wizard_horizontal">
									<div id="step-1">
										<div class="form-horizontal form-label-left">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre del Plan<span class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="text" id="plan" class="form-control col-md-7 col-xs-12" value="<?php echo $plan[0]->mv;?>">
													<small id="msj_plan"></small>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripcion del Plan<span class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<textarea class="form-control col-md-7 col-xs-12" id="descripcion"><?php echo $plan[0]->descripcion;?></textarea>
													<small id="msj_descripcion"></small>
												</div>
											</div>
											<input id="idMv" value="<?php echo $plan[0]->idMv;?>" style="display: none;">

										</div>
									</div>
								</div>
								<!-- End SmartWizard Content -->
								<div class="row" id="seccionBotones">
									<div class="col-md-12 text-center" id="btnActualizar">
										<button class="btn btn-success">Actualizar</button>
									</div>
								</div>
								<!-- End SmartWizard Content -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /page content -->
		<?php require_once 'complementos/footer.php'?>
