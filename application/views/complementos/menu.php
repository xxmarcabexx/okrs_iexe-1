<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
		<div class="navbar nav_title" style="border: 0; background: #003A5D;">
			<!--<text class="site_title"><span>Tablero de Control</span></text>-->
			<img src="<?php echo base_url(); ?>assets/build/images/logo_menu.jpg" class="img-responsive">

		</div>
		<div class="clearfix"></div>
		<div class="profile clearfix">
			<div class="clearfix"></div>
		</div>
		<br/>
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3>Menu Principal</h3>
				<?php if ($this->session->userdata('tipo') != 'lider') { ?>
					<ul class="nav side-menu">

						<?php if($this->session->userdata('tipo')=='superadmin'){ ?>
							<li><a><i class="fa fa-user"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a href="<?php echo base_url(); ?>lista_usuarios"><i class="fa fa-users"></i>Lista de Usuarios</a></li>
									<li><a href="<?php echo base_url(); ?>nuevo_usuario"><i class="fa fa-user-plus"></i>Nuevo Usuario</a></li>
								</ul>
							</li>
						<?php } ?>

						<li><a><i class="fa fa-edit"></i> Planes <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url(); ?>lista_planes">Lista de Planes</a></li>
								<?php if($this->session->userdata('tipo')!='capturista'){ ?>
								<li><a href="<?php echo base_url(); ?>nuevo_plan">Nuevo Plan</a></li>
								<?php } ?>
							</ul>
						</li>
						<li><a><i class="fa fa-edit"></i> Objetivos <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url(); ?>lista_objetivos">Lista de Objetivos</a></li>
								<?php if($this->session->userdata('tipo')!='capturista'){ ?>
								<li><a href="<?php echo base_url(); ?>nuevo_objetivo">Nuevo Objetivo</a></li>
								<?php } ?>
							</ul>
						</li>
                        <li><a><i class="fa fa-edit"></i> Acuerdos <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo base_url(); ?>lista_minutas">Lista de Acuerdos</a></li>
                                <?php if($this->session->userdata('tipo')!='capturista'){ ?>
                                    <li><a href="<?php echo base_url(); ?>nueva_minuta">Nuevo Acuerdo</a></li>
                                <?php } ?>
                            </ul>
                        </li>
					</ul>
				<?php } elseif ($this->session->userdata('tipo')=='lider') { ?>
					<ul class="nav side-menu">
						<li>
							<a href="<?php echo base_url() ?>admin">
								<i class="fa fa-home"></i>
								Mis compromisos
							</a>
						</li>
						<?php foreach ($planes as $plan) { ?>
						<li>
							<a>
								<i class="fa fa-sitemap"></i>
								<text class="planes" title="<?php echo $plan->idMv; ?>"><?php echo $plan->mv;?></text>
								<span class="fa fa-chevron-down"></span>
							</a>
							<ul class="nav child_menu">
								<?php foreach ($plan->objetivos as $objetivos){?>
									<li><a><text class="objetivos" title="<?php echo $objetivos->idObjetivo; ?>"><?php echo $objetivos->objetivo;?></text>
											<span class="fa fa-chevron-down"></span>
										</a>
										<ul class="nav child_menu">
											<?php foreach ($objetivos->kr as $kr){?>
												<li class="sub_menu"><a><text class="kr" title="<?php echo $kr->idKeyResult; ?>"><?php echo $kr->descripcion;?></text></a></li>
											<?php } ?>
										</ul>
									</li>
								<?php } ?>
							</ul>
						</li>
						<?php } ?>
                        <li>
                            <a>
                                <i class="fa fa-sitemap"></i>
                                <text id="acuerdos">Acuerdos</text>
                                <span class="fa fa-chevron-down"></span>
                            </a>
                        </li>
					</ul>
				<?php } ?>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready(function () {

		$(".planes").click(function () {
			$.ajax({
				type: "POST",
				url: 'detalle_plan/'+$(this).attr('title'),
				data:{},
				success: function(datos){
					$('#carga').empty();
					$('#carga').html(datos);
				}
			});
		});

        $("#acuerdos").click(function () {
            $.ajax({
                type: "POST",
                url: 'MinutasController/gobernador',
                data:{},
                success: function(datos){
                    $('#carga').empty();
                    $('#carga').html(datos);
                }
            });
        });


		$(".objetivos").click(function () {
			$.ajax({
				type: "POST",
				url: 'detalle_objetivo/'+$(this).attr('title'),
				data:{},
				success: function(datos){
					$('#carga').empty();
					$('#carga').html(datos);
				}
			});
		});

		$(".kr").click(function () {
			$.ajax({
				type: "POST",
				url: 'detalle_keyresult/'+$(this).attr('title'),
				data:{},
				success: function(datos){
					$('#carga').empty();
					$('#carga').html(datos);
				}
			});
		});




	});
</script>
