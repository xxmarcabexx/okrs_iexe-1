<?php require_once 'complementos/head.php'?>

<script src="<?php echo base_url();?>assets/build/js/edita_objetivos.js"></script>
<link href="<?php echo base_url();?>assets/build/css/edita_objetivo.css" rel="stylesheet">


<link href="<?php echo base_url();?>assets/build/datepicker/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>assets/build/datepicker/js/datepicker.min.js"></script>
<!-- Include English language -->
<script src="<?php echo base_url();?>assets/build/datepicker/js/i18n/datepicker.es.js"></script>
</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php'?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php'?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Modulo Objetivos</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Edición Objetivos<small></small></h2>
								<div class="clearfix"></div>
								<input id="idObjetivo" value="<?php echo $idObjetivo;?>" style="display: none;">
							</div>
							<div class="x_content">
								<!-- Smart Wizard -->
								<div class="form-horizontal form-label-left" id="formularioEdita">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Misión / Visión<span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input type="text" readonly class="form-control col-md-7 col-xs-12" value="<?php echo $objetivo[0]->mv;?>">
											<small id="msj_objetivo" class="msj"></small>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Objetivo<span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input type="text" id="objetivo" class="form-control col-md-7 col-xs-12" value="<?php echo $objetivo[0]->objetivo;?>">
											<small id="msj_objetivo" class="msj"></small>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripcion del objetivo <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<textarea class="form-control col-md-7 col-xs-12" id="descripcion"><?php echo $objetivo[0]->descripcion;?></textarea>
											<small id="msj_descripcion" class="msj"></small>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecha de Inicio del Objetivo<span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input data-language='es' type="text" id="inicio"  class="datepicker-here form-control col-md-7 col-xs-12" value="<?php echo $objetivo[0]->finicio;?>">
											<small id="msj_inicio" class="msj"></small>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecha Final del Objetivo<span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input  data-language='es' type="text" id="final"  class="datepicker-here form-control col-md-7 col-xs-12" value="<?php echo $objetivo[0]->ffinal;?>">
											<small id="msj_final" class="msj"></small>
										</div>
									</div>
									<div class="form-group text-center">
										<button class="btn btn-success" id="btnEditar">Guardar</button>
									</div>

								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php require_once 'complementos/footer.php'?>
