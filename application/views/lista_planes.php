<?php
require_once 'complementos/head.php';
header('Access-Control-Allow-Origin: *');
?>

<script src="<?php echo base_url(); ?>assets/build/js/lista_planes.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/lista_planes.css" rel="stylesheet">

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <?php require_once 'complementos/menu.php' ?>

        <!-- top navigation -->
        <?php require_once 'complementos/topnavigation.php' ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <img src="<?php echo base_url(); ?>assets/build/images/mp.png" class="img-responsive">
                        <!--<h3>Modulo Planes</h3>-->
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="clearfix"></div>
                            </div>
                            <?php
                            if ($planes == null){
                                echo "<h4>Sin asignación.</h4>";
                            }
                            else
                            foreach ($planes

                            as $plan){ ?>
                            <div class="x_content" id="listaPlanes<?php echo $plan->idMv; ?>">
                                <div class="col-md-8">
                                    <!-- start accordion -->
                                    <div class="tituloObjetivos row accordion-toggle">
                                        <div class="row alertAvance">
                                            <div class="col-md-12">
                                                <?php if (isset($plan->totalActualizar) && $plan->totalActualizar > 0) { ?>
                                                    <sup
                                                            style="background: #ED2B00 ; font-size: 12px; border-radius: 50px !important; color: white; padding: 4px 7px; margin-left: 3px;">
                                                        <?php echo $plan->totalActualizar; ?>
                                                    </sup>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 centrado">
                                            <div class="col-md-8 ico" data-toggle="collapse"
                                                 href="#<?php echo $plan->idMv; ?>">
                                                <h2>
                                                    <i class="fa fa-arrow-circle-down iconito" style="font-size: 25px;"
                                                       aria-hidden="true"></i>
                                                    <?php echo $plan->mv ?>
                                                </h2>
                                            </div>
                                            <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                <?php if ($this->session->userdata('tipo') == 'admin') { ?>
                                                    <div class="col-md-4 text-left" style="margin-top: 7px;">
                                                        <button class="btn btn-primary btn-xs editaPlan"
                                                                onclick="editarPlan('<?php echo $plan->idMv; ?>')">
                                                            Editar
                                                            Plan
                                                        </button>
                                                    </div>
                                                <?php } elseif ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                    <div class="col-md-2 text-left" style="margin-top: 7px;">
                                                        <button class="btn btn-primary btn-xs editaPlan"
                                                                onclick="editarPlan('<?php echo $plan->idMv; ?>')">
                                                            Editar
                                                            Plan
                                                        </button>
                                                    </div>
                                                    <div class="col-md-2 text-left" style="margin-top: 7px;">
                                                        <button class="btn btn-danger btn-xs eliminaPlan"
                                                                value='<?php echo $plan->idMv; ?>'>
                                                            Eliminar
                                                        </button>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>

                                        <!--<div class="col-md-5 col-sm-8 col-xs-12">
										<div class="project_progress">
											<small><?php echo number_format($plan->avance, 2); ?>% Progreso OkR</small>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="<?php echo $plan->avance; ?>"
													 aria-valuenow="56" style="width: 100%;"></div>
											</div>
										</div>
									</div>-->

                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <h6>Progreso OKR</h6>
                                    <div id="gauge<?php echo $plan->idMv; ?>" class="grafi"></div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <h6>Progreso Indicadores</h6>
                                    <div id="gaugeIndi<?php echo $plan->idMv; ?>" class="grafi"></div>
                                </div>
                                <div class="col-md-12">
                                    <div id="<?php echo $plan->idMv; ?>" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <?php if ($this->session->userdata('tipo') != 'capturista'){ ?>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <?php }else{ ?>
                                                <div class="col-md-12">
                                                    <?php } ?>
                                                    <table class="table table-striped table-condensed resp_table demo">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-left th_planes" id="th_indicador"><i
                                                                        class="fa fa-caret-square-o-right"
                                                                        aria-hidden="true"></i>
                                                                Indicador
                                                            </th>
                                                            <th class="text-left th_planes" id="th_avance"><i
                                                                        class="fa fa-line-chart" aria-hidden="true"></i>
                                                                Avance
                                                            </th>
                                                            <th class="text-left th_planes" id="th_progreso"><i
                                                                        class="fa fa-gear" aria-hidden="true"></i>
                                                                Progreso
                                                            </th>
                                                            <th class="text-left th_planes" id="th_vi"><i
                                                                        class="fa fa-percent" aria-hidden="true"></i>
                                                                Valor
                                                                Inicial
                                                            </th>
                                                            <th class="text-left th_planes" id="th_vf"><i
                                                                        class="fa fa-percent" aria-hidden="true"></i>
                                                                Valor
                                                                Final
                                                            </th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        //if (count($plan) > 0)
                                                        foreach ($plan->indicadores as $indic) { ?>
                                                            <tr>
                                                                <td data-th="Indicador"><?php echo $indic->nombreIndicador; ?></td>
                                                                <td data-th="Avance"
                                                                    id="avance<?php echo $plan->idMv ?>"><?php echo $indic->avanceReal; ?></td>
                                                                <td data-th="Progreso">
                                                                    <div class="project_progress">
                                                                        <small
                                                                                id="smallindicador<?php echo $indic->idIndicadores ?>">
                                                                            <?php echo number_format($indic->avance, 2); ?>
                                                                            %
                                                                        </small>
                                                                        <?php
                                                                        if (count($indic->bitacora) > 0 && $indic->bitacora[0]->aprobado == 0) { ?>
                                                                            <small style="color: red;"
                                                                                   id="marcadorAvance<?php echo $indic->idIndicadores; ?>">
                                                                                ( <?php echo $indic->bitacora[0]->avance; ?>
                                                                                % sin autorizar )
                                                                            </small>
                                                                        <?php } else { ?>
                                                                            <small
                                                                                    id="marcadorAvance<?php echo $indic->idIndicadores; ?>"></small>
                                                                        <?php } ?>
                                                                        <div class="progress progress_sm">
                                                                            <div
                                                                                    id="indicador<?php echo $indic->idIndicadores; ?>"
                                                                                    class="progress-bar bg-green"
                                                                                    role="progressbar"
                                                                                    data-transitiongoal="<?php echo $indic->avance; ?>"
                                                                                    aria-valuenow="56"
                                                                                    style="width: 57%;"></div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td id="indiinicio<?php echo $indic->idIndicadores; ?>"
                                                                    data-th="Inicio"><?php echo $indic->inicio; ?></td>
                                                                <td id="indifinal<?php echo $indic->idIndicadores; ?>"
                                                                    data-th="Final"><?php echo $indic->final; ?></td>
                                                                <td>
                                                                    <?php if ($indic->avance == $indic->final) { ?>
                                                                        <p>Completado 100%</p>
                                                                    <?php } else { ?>
                                                                        <button
                                                                                class="btn btn-info btn-xs updateAvance"
                                                                                id="uAvance<?php echo $indic->idIndicadores; ?>"
                                                                                value="<?php echo $indic->idIndicadores; ?>">
                                                                            <i class="fa fa-pencil"></i> Actualizar
                                                                        </button>
                                                                    <?php } ?>
                                                                    <td>
                                                                        <button
                                                                                class="btn btn-danger btn-xs bitacoraLista"
                                                                                id="bitacora<?php echo $indic->idIndicadores; ?>"
                                                                                value="<?php echo $indic->idIndicadores; ?>">
                                                                            <i class="fa fa-pencil"></i> Bitácora
                                                                        </button>
                                                                    </td>
                                                                </td>
                                                                <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                    <td>
                                                                        <?php
                                                                        if (count($indic->bitacora) > 0) {
                                                                            if ($indic->bitacora[0]->aprobado == 2) {
                                                                                echo
                                                                                    "<button value='$indic->idIndicadores' class='btn btn-warning btn-xs avanceCancelado' id='avanceCancelado" . $indic->idIndicadores . "'>
																						<i class='fa fa-pencil'></i> No autorizado
																					</button>";
                                                                            } elseif ($indic->bitacora[0]->aprobado == 0) {
                                                                                echo
                                                                                    "<button class='btn btn-danger btn-xs autorizaAvance' id='autorizaAvance" . $indic->idIndicadores . "' value=" . $indic->bitacora[0]->idBitacora . ">
																						<i class='fa fa-pencil'></i> Autorizar
																					</button>";
                                                                            }
                                                                        } ?>
                                                                    </td>
                                                                <?php } elseif ($this->session->userdata('tipo') == 'capturista') { ?>
                                                                    <td>
                                                                        <?php
                                                                        if (count($indic->bitacora) > 0) {
                                                                            if ($indic->bitacora[0]->aprobado == 2) {
                                                                                echo
                                                                                    "<button value='$indic->idIndicadores' class='btn btn-warning btn-xs avanceCancelado' id='avanceCancelado" . $indic->idIndicadores . "'>
																						<i class='fa fa-pencil'></i> No autorizado
																					</button>";
                                                                            }
                                                                        } ?>
                                                                    </td>
                                                                <?php } ?>
                                                                <?php if ($this->session->userdata('tipo') != 'superadmin') { ?>
                                                                    <td>
                                                                        <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                            <button
                                                                                    class='btn btn-warning btn-xs updateIndicador'
                                                                                    value='<?php echo $indic->idIndicadores; ?>'>
                                                                                <i class='fa fa-pencil'></i> Editar
                                                                            </button>
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php
                                                                        if ($this->session->userdata('tipo') != 'capturista')
                                                                            if (count($indic->bitacoraTemp) > 0) {
                                                                                echo ($indic->bitacoraTemp[0]->status == 1) ? "" : "<button class='btn btn-danger btn-xs updateSiIndicador' value=" . $indic->idIndicadores . " ><i class='fa fa-pencil'></i> Autorizar actualización</button>";
                                                                            }
                                                                        ?>
                                                                    </td>
                                                                <?php } ?>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end of accordion -->
                                    </div>
                                </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="tituloIndicador"></h4>

                        </div>


                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="tipometrica"></span></label>
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="actual"></span></label>
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="rango"></span></label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Avance</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" id="avance" class="form-control col-md-7 col-xs-12" min="0"
                                           step="0.5">
                                    <small id="msj_avance"></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción del
                                    avance<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control col-md-7 col-xs-12" id="descripcionavance"></textarea>
                                    <small id="msj_descripcionavance"></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-success btn-xs agregaAnexo"><i class="fa fa-paperclip"></i>
                                        Adjuntar evidencia
                                    </button>
                                </div>
                                <div class="col-md-12 text-center">
                                    <small id="mensajeAexos" style="color: red;"></small>
                                </div>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="project_progress" id="barraCarga" style="display: none;">
                                        <small id="porcentajeProgreso">
                                        </small>
                                        <div class="progress progress_sm">
                                            <small id="porcentajeProgreso" style="color: red;"></small>
                                            <div class="progress-bar bg-green" id="barraProgresoCarga"
                                                 role="progressbar" data-transitiongoal="20" aria-valuenow="56"
                                                 style="width: 57%;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="row" id="formAnexo"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="btnAceptar">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                        aria-label="Close" id="btnFinalizar">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="modal fade" id="aprobadoCancel" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label>El indicador tiene un avance que aun no ha sido validado</label>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modalValida" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="indicadorTa"></h4>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label id="mensajeValida"></label>
                            </div>
                            <!--<div class="form-group">
                                <label id="capturista"></label>
                            </div>-->
                            <div class="form-group">
                                <label id="avanceValida"></label>
                            </div>
                            <div class="form-group">
                                <label id="descripcionValida"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <table class="table table-striped table-bordered" id="tablaAnexos">
                                    <thead>
                                    <td>Nombre de la evidencia</td>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-4 text-right">
                                <button class="btn btn-primary" id="btnAceptarValida">Autorizar</button>
                            </div>
                            <div class="col-md-4 text-center">
                                <button class="btn btn-danger" id="btnNoAutorizar" class="close" data-dismiss="modal"
                                        aria-label="Close">No autorizar
                                </button>
                            </div>
                            <div class="col-md-4 text-left">
                                <button class="btn btn-danger" class="close" data-dismiss="modal"
                                        aria-label="Close">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modalClave" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-grou>
								<label id=" leyendaEliminar
                            ">Esta apunto de eliminar un plan</label><br>
                            <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                        </div>
                        <div class="form-group">
                            <input type="password" id="contraseña">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="validaClave">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                    aria-label="Close">Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalClaveEditar" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-grou>
								<label id=" leyendaEliminar
                        ">Esta apunto de editar un indicador</label><br>
                        <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                    </div>
                    <div class="form-group">
                        <input type="password" id="contraseñaUpdate">
                    </div>
                </div>
                <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                    <div class="col-md-6 text-right">
                        <button class="btn btn-primary" id="validaClaveEditar">Aceptar</button>
                    </div>
                    <div class="col-md-6 text-left">
                        <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                aria-label="Close">Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalIndicador" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <h4>Favor de ingrsar los nuevos valores</h4>
                    <small><b>Nota</b>: Una vez aprobado el cambio por el administrador, los valores en avance sera
                        reiniciado a 0
                    </small>
                    <br><br><br>
                    <div class="col-md-4 col-md-offset-2">
                        <input type="number" class="form-control has-feedback-left" id="vinicial"
                               placeholder="Valor Inicial">
                        <span class="fa fa-flag-checkered form-control-feedback left"
                              aria-hidden="true"></span>
                        <small id="msj_vinicial"></small>
                    </div>
                    <div class="col-md-4">
                        <input type="number" class="form-control has-feedback-left" id="vfinal"
                               placeholder="Valor Final">
                        <span class="fa fa-flag-checkered form-control-feedback left"
                              aria-hidden="true"></span>
                        <small id="msj_vfinal"></small>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 25px; margin-top: 80px !important;">
                    <div class="col-md-6 text-right">
                        <button class="btn btn-primary" id="btnAceptarEditarIndicador">Aceptar</button>
                    </div>
                    <div class="col-md-6 text-left">
                        <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                aria-label="Close">Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="updateCancel" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <div class="form-group">
                        <label>El indicador ya cuenta con una actualización</label>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="noAutorizar" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">No autorizar el avance de: <label id="avanceDe"></label></h4>

                </div>

                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <div class="form-group">
                        <label id="motivoCancel"></label>
                    </div>
                </div>
                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <div class="form-group">
                        <label>Motivo: </label>
                    </div>
                </div>
                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <div class="form-group">
                        <textarea id="motivo"></textarea>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                    <div class="col-md-6 text-center">
                        <button class="btn btn-danger" data-dismiss="modal" id="aceptarCancelacion" aria-label="Close">
                            Aceptar
                        </button>
                    </div>
                    <div class="col-md-6 text-center">
                        <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalCancelado" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center" id="tituloCancelado"></h4>

                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h5 id="usuarioCancelado"></h5>
                    </div>
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h5 id="avanceCancelado"></h5>
                    </div>
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h5 id="motivoCancelado"></h5>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                    <div class="col-md-6 text-right">
                        <button class="btn btn-primary" id="btnAceptarCancelado">Aceptar</button>
                    </div>
                    <div class="col-md-6 text-left">
                        <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                aria-label="Close" id="btnFinalizar">Cerrar
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
           style="display: none;">
    <input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>" style="display: none;">
    <input id="fechaHoy" value="<?php echo date('Y-m-d H:i'); ?>" style="display: none;">


    <div class="modal fade" id="modalBitacora" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center" id="indicadorT">Bitácora del indicador: <strong id="tIndicador"></strong></h4>
                </div>

                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 text-center">
                            <table class="table table-striped table-bordered" id="tablaBitacora">
                                <thead style="font-size: 12px;">
                                    <td>Fecha</td>
                                    <td>Descripción</td>
                                    <td>Último avance</td>
                                    <td>Avance</td>
                                    <td>Estatus</td>
                                    <!--<td>Usuario/autorizo</td>-->
                                    <td>Usuario</td>
                                    <td>Motivo</td>
                                    <td>Adjuntos</td>
                                    <!--<td>Usuario/no autorizo</td>-->
                                </thead>
                                <tbody style="font-size: 10px;">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require_once 'complementos/footer.php' ?>


    <script>
        $(document).ready(function () {
            <?php foreach ($planes as $plan){ ?>
            g = new JustGage({
                id: 'gauge<?php echo $plan->idMv;?>',
                value: <?php echo $plan->avance; ?>,
                min: 0,
                max: 100,
                titlePosition: "below",
                valueFontColor: "#3f4c6b",
                pointer: true,
                pointerOptions: {
                    toplength: -15,
                    bottomlength: 10,
                    bottomwidth: 12,
                    color: '#8e8e93',
                    stroke: '#ffffff',
                    stroke_width: 3,
                    stroke_linecap: 'round'
                },
                relativeGaugeSize: true,

            });

            <?php } ?>


            <?php foreach ($planes as $plan){ ?>
            g = new JustGage({
                id: 'gaugeIndi<?php echo $plan->idMv;?>',
                value: <?php echo $plan->avanceIndicadores; ?>,
                min: 0,
                max: 100,
                titlePosition: "below",
                valueFontColor: "#3f4c6b",
                pointer: true,
                pointerOptions: {
                    toplength: -15,
                    bottomlength: 10,
                    bottomwidth: 12,
                    color: '#8e8e93',
                    stroke: '#ffffff',
                    stroke_width: 3,
                    stroke_linecap: 'round'
                },
                relativeGaugeSize: true,

            });

            <?php } ?>

        });


    </script>
