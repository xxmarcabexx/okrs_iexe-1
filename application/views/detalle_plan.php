<?php //require_once 'complementos/head.php' ?>
<?php
header('Access-Control-Allow-Origin: *');


?>


<script src="<?php echo base_url(); ?>assets/build/js/detalle_plan.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/detalle_plan.css" rel="stylesheet">
<style>

    .pf {
        padding: 10px 15px;
        background: #f5f5f5;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #00AEAA;
        border-left: 1px solid #00AEAA;
        border-right: 1px solid #00AEAA;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }

    .panel {
        margin-bottom: 0 !important;
    }

    /*****************Chat******************/
    .panel-primary {
        border-color: #00AEAA !important;
    }

    .panel-primary > .panel-heading {
        color: #fff;
        background-color: #00AEAA;
        border-color: #00AEAA;
    }

    .enviarChat {
        background: #00AEAA;
    }

    .PlanChat {
        height: 350px !important;
        background-image: url('assets/build/images/chat.jpg');
        overflow: scroll;
    }

    .mensajeChatDer, .mensajeChatIzq {
        margin-right: 0px !important;
        background: white !important;
        padding: 5px 10px !important;
        margin-bottom: 10px !important;
        border-radius: 7px !important;
    }

    .mensajeChatDer {
        float: right !important;
    }

    .mensajeChatDer label {
        color: orange;
    }

    .mensajeChatIzq label {
        color: crimson;
    }

    ul {
        list-style-type: none;

    }

    /***************************************************/

    .tituloPlanes {
        cursor: pointer !important;
        background: #00AEAA !important;
        border-radius: 4px !important;
        color: white !important;
        padding: 10px 0px 10px 0px !important;
    }

    .tituloPlanes h2 {
        color: white !important;
    }



    .acordingPlanes {
        cursor: pointer !important;
        background: #00AEAA !important;
        border-radius: 4px !important;
        color: white !important;
        padding: 0px 0px 0px 0px !important;
    }

    .acordingPlanes h4 {
        color: white !important;
    }

</style>
</head>

<!--<body class="nav-md">
<div class="container body">
	<div class="main_container">-->
<?php //require_once 'complementos/menu.php' ?>
<?php //require_once 'complementos/topnavigation.php' ?>
<!-- /top navigation -->
<!--<div class="right_col" role="main">-->
<div class="">
    <div class="page-title">
        <div class="title_left">
            <img src="<?php echo base_url(); ?>assets/build/images/mp.png" class="img-responsive">

        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- start accordion -->
                    <input id="idPlan" value="<?php echo $pl[0]->idMv; ?>" style="display: none">
                    <div class="tituloPlanes row accordion-toggle" data-toggle="collapse" href="#">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12">
                                <h2><?php echo $pl[0]->mv ?></h2>
                            </div>
                        </div>
                        <!--<div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="project_progress">
                                <small>2% Complete</small>
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-blue" role="progressbar"
                                         data-transitiongoal="3" aria-valuenow="56"
                                         style="width: 100%;"></div>
                                </div>
                            </div>
                        </div>-->

                    </div>

                <div id="">
                    <div class="clearfix"></div>
                    <div id="objetivoAnual" class="col-md-5">
                        <div class="col-md-12 subtitulos text-center">
                            <h3>Objetivo Anual</h3>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        if($objAnual!=0) {
                            foreach ($objAnual as $oa) {
                                ?>
                                <div style="cursor: pointer; margin-top: 20px; margin-left: 0%;"
                                     title="<?php echo $oa->idObjetivo; ?>"
                                     class="col-md-7 col-md-offset-1 col-sm-3 col-xs-12 bg-white text-center graficasP objetivoss">
                                    <div id='anual<?php echo $oa->idObjetivo; ?>' class="2000x1600px"></div>
                                    <h5 style="font-weight: normal; font-size: 12px;"><?php echo (strlen($oa->descripcion)>100) ? substr($oa->descripcion, 0, 100)."..." : $oa->descripcion; ?></h5>
                                </div>
                            <?php }
                        }else{
                        ?>
                            <h2>El plan no cuenta con aún con un objetivo anual</h2>
                        <?php } ?>
                    </div>

                    <div class="col-md-7">
                        <div class="col-md-12 subtitulos text-center">
                            <h3>OKR Bimestral</h3>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        if($krAnual!=0)
                        foreach ($krAnual as $kra) {
                            ?>
                            <div style="cursor: pointer; margin-top: 20px;" title="<?php echo $kra->idKeyResult; ?>" class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12 bg-white text-center graficas krInt">
                                <div id='kranual<?php echo $kra->idKeyResult; ?>' class="2000x1600px"></div>
                                <h5 style="font-weight: normal; font-size: 12px;"><?php echo (strlen($kra->descripcion)>90) ? substr($kra->descripcion, 0, 90)."..." : $kra->descripcion; ?></h5>
                            </div>
                        <?php } ?>
                    </div>

                    <?php if(count($obj)>0){?>
                        <!--Informacion de la ficha tecnica-->
                        <div class="col-md-12 col-sm-12 col-xs-12 acordingPlanes" style="margin-top: 15px; margin-top: 45px;">
                            <div class="col-md-8 ico" data-toggle="collapse" href="#objetivos">
                                <h5>
                                    <i class="fa fa-plus-circle iconito" style="font-size: 15px;"
                                       aria-hidden="true"></i>
                                    Objetivos
                                </h5>
                            </div>
                        </div>
                        <div id="objetivos" class="accordion-body collapse">
                            <div class="clearfix"></div>
                            <?php
                            foreach ($obj as $objetivo) {
                                ?>
                                <div style="cursor: pointer; margin-top: 20px;" title="<?php echo $objetivo->idObjetivo; ?>" class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12 bg-white text-center graficas objetivoss">
                                    <div id='prueba<?php echo $objetivo->idObjetivo; ?>' class="2000x1600px"></div>
                                    <h5><?php echo $objetivo->descripcion; ?></h5>
                                </div>
                            <?php } ?>
                        </div>
                        <div id="objetivos" class="accordion-body collapse">
                            <div class="clearfix"></div>
                            <?php
                            foreach ($obj as $objetivo) {
                                ?>
                                <div style="cursor: pointer; margin-top: 20px;" title="<?php echo $objetivo->idObjetivo; ?>" class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12 bg-white text-center graficas objetivoss">
                                    <div id='prueba<?php echo $objetivo->idObjetivo; ?>' class="2000x1600px"></div>
                                    <h5><?php echo $objetivo->descripcion; ?></h5>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>


                    <div class="col-md-12 col-sm-12 col-xs-12 acordingPlanes" style="margin-top: 15px; margin-top: 45px;">
                        <div class="col-md-8 ico" data-toggle="collapse" href="#indicadores">
                            <h5>
                                <i class="fa fa-plus-circle iconito" style="font-size: 15px;"
                                   aria-hidden="true"></i>
                                Indicadores
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-12 subtitulos accordion-body collapse" id="indicadores">
                        <div class="col-md-4">
                            <h4>Presupuesto 2018</h4>
                            <p>$<?php echo $pl[0]->presupuesto2018; ?></p>
                        </div>
                        <div class="col-md-4">
                            <h4>Presupuesto 2019</h4>
                            <p><?php echo $pl[0]->presupuesto2019; ?></p>
                        </div>
                        <div class="col-md-4">
                            <h4>Indicador estratégico</h4>
                            <p>$<?php echo $pl[0]->indicadorEstrategico; ?></p>
                        </div>

                        <div class="col-md-4">
                            <h4>Inversión 2018</h4>
                            <p>$<?php echo $pl[0]->inversion2018; ?></p>
                        </div>
                        <div class="col-md-4">
                            <h4>Inversión 2019</h4>
                            <p>$<?php echo $pl[0]->inversion2019; ?></p>
                        </div>
                        <div class="col-md-4">
                            <h4>Meta del indicador</h4>
                            <p><?php echo $pl[0]->metaIndicador; ?></p>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 acordingPlanes" style="margin-top: 15px; margin-top: 45px;">
                        <div class="col-md-8 ico" data-toggle="collapse"
                             href="#detalle">
                            <h5>
                                <i class="fa fa-plus-circle iconito" style="font-size: 15px;"
                                   aria-hidden="true"></i>
                                Descripción del proyecto
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-12 subtitulos accordion-body collapse" id="detalle">
                        <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-md-6">
                                <h4>Nombre del Proyecto</h4>
                                <p><?php echo $pl[0]->mv; ?></p>
                            </div>
                            <div class="col-md-6">
                                <h4>Status</h4>
                                <p><?php echo $pl[0]->status ?></p>
                            </div>
                            <br>
                            <!--Informacion de la ficha tecnica-->
                            <div class="col-md-6">
                                <h4>Líder del proyecto</h4>
                                <p><?php echo $pl[0]->lider; ?></p>
                            </div>
                            <div class="col-md-6">
                                <h4>Inversión total</h4>
                                <p>$<?php echo $pl[0]->inversionT ?></p>
                            </div>
                            <div class="col-md-6">
                                <h4>Fin</h4>
                                <p><?php echo $pl[0]->fin ?></p>
                            </div>
                            <div class="col-md-6">
                                <h4>Población potencial</h4>
                                <p><?php echo $pl[0]->poblacionPotencial ?></p>
                            </div>
                            <div class="col-md-6">
                                <h4>Propósito</h4>
                                <p><?php echo $pl[0]->proposito; ?></p>
                            </div>
                            <div class="col-md-6">
                                <h4>Población objetivo</h4>
                                <p><?php echo $pl[0]->poblacionObjetivo; ?></p>
                            </div>
                        </div>
                    </div>



                    <!--<div class="col-md-12 subtitulos">
                        <?php foreach ($indicadores as $ind) { ?>
                            <div class="col-md-6">
                                <h4><?php echo $ind->nombreIndicador; ?></h4>
                                <small style="color: #00AEAA;">
                                    <?php echo $ind->avancePorcentaje; ?>%
                                </small>
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-green" role="progressbar"
                                         data-transitiongoal="30" aria-valuenow="56"
                                         style="width: <?php echo $ind->avancePorcentaje; ?>%;"></div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-12 text-justify">
                            <h4>Resultados del mes</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever
                                since the 1500s, when an unknown printer took a galley of type and
                                scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged. It was popularised in the 1960s with the release
                                of Letraset sheets containing Lorem Ipsum passages, and more recently
                                with desktop publishing software like Aldus PageMaker including versions
                                of Lorem Ipsum.</p>
                        </div>
                    </div>-->
                </div>
                <!-- end of accordion -->
            </div>

        </div>
    </div>
</div>

<input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
       style="display: none;">
<input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>" style="display: none;">
<input id="fechaHoy" value="<?php echo date('Y-m-d H:i'); ?>" style="display: none;">
</div>
<!--</div>-->
<?php //require_once 'complementos/footer.php' ?>

<script>
    $(document).ready(function () {
        <?php
        if($krAnual!=0)
        foreach ($krAnual as $kra) {
        ?>
        var g = new JustGage({
            id: "kranual<?php echo $kra->idKeyResult;?>",
            value:<?php echo $kra->avancePorcentaje;?>,
            min: 0,
            max: 100,
            titlePosition: "below",
            valueFontColor: "#3f4c6b",
            pointer: true,
            pointerOptions: {
                toplength: -15,
                bottomlength: 10,
                bottomwidth: 12,
                color: '#8e8e93',
                stroke: '#ffffff',
                stroke_width: 3,
                stroke_linecap: 'round'
            },
            relativeGaugeSize: true,

        });
        <?php } ?>

        <?php
        foreach ($obj as $objetivo) {
        ?>
        var g = new JustGage({
            id: "prueba<?php echo $objetivo->idObjetivo;?>",
            value:<?php echo $objetivo->avance;?>,
            min: 0,
            max: 100,
            titlePosition: "below",
            valueFontColor: "#3f4c6b",
            pointer: true,
            pointerOptions: {
                toplength: -15,
                bottomlength: 10,
                bottomwidth: 12,
                color: '#8e8e93',
                stroke: '#ffffff',
                stroke_width: 3,
                stroke_linecap: 'round'
            },
            relativeGaugeSize: true,

        });
        <?php } ?>

        <?php
        if($objAnual!=0)

        foreach ($objAnual as $oa) {
        ?>
        var g = new JustGage({
            id: "anual<?php echo $oa->idObjetivo;?>",
            value:<?php echo $oa->avance;?>,
            min: 0,
            max: 100,
            titlePosition: "below",
            valueFontColor: "#3f4c6b",
            pointer: true,
            pointerOptions: {
                toplength: -15,
                bottomlength: 10,
                bottomwidth: 12,
                color: '#8e8e93',
                stroke: '#ffffff',
                stroke_width: 3,
                stroke_linecap: 'round'
            },
            relativeGaugeSize: true,

        });
        <?php } ?>

        $(".acordingPlanes").click(function () {
            if($(this).find("i").hasClass('fa fa-plus-circle iconito')){
                $(this).find("i").removeClass('fa fa-plus-circle iconito');
                $(this).find("i").addClass('fa fa-arrow-circle-up iconito');
            }else if($(this).find("i").hasClass('fa fa-arrow-circle-up iconito')){
                $(this).find("i").removeClass('fa fa-arrow-circle-up iconito');
                $(this).find("i").addClass('fa fa-plus-circle iconito');
            }

        });



        $(".objetivoss").click(function () {
            $.ajax({
                type: "POST",
                url: 'detalle_objetivo/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                },
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr() ;
                    xhr.onloadstart = function(e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr ;
                }
            });
        });

        $(".krInt").click(function () {
            $.ajax({
                type: "POST",
                url: 'KeyResultController/detalleKrPrincipal/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                },
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr() ;
                    xhr.onloadstart = function(e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr ;
                }
            });
        });



    });
</script>
