<!DOCTYPE html>
<html>
<head>
	<title>Tablero</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="assets/build/css/style.css" rel='stylesheet' type='text/css'/>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!--/web-fonts-->
</head>
<script>
	$(document).ready(function () {
		$("#usuario").click(function () {
			$(this).val('');
			$("#usuario").css({"background": "white"});
			$("#msj_usuario").fadeOut(1500);
		});
		$("#clave").click(function () {
			$(this).val('');
			$("#clave").css({"background": "white"});
			$("#msj_clave").fadeOut(1500);
		});

		$(document).keypress(function (e) {
			if (e.which == 13) {
				var avanza = 1;
				var usuario = $("#usuario").val();
				var clave = $("#clave").val();
				if (usuario == '') {
					$("#msj_usuario").html("Ingrese su usuario");
					$("#usuario").css({"background": "#EFD3D2"});
					$("#msj_usuario").show();
					avanza = 0;
				}
				if (clave == '') {
					$("#msj_clave").html("Ingrese su clave");
					$("#clave").css({"background": "#EFD3D2"});
					$("#msj_clave").show();
					avanza = 0;
				}
				if (avanza == 1){
					$.ajax({
						url: 'LoginController/valida',
						data: {
							user: usuario,
							clave: clave
						},
						type: 'POST',
						success: function (response) {
							respuesta = JSON.parse(response);
							if (respuesta['ingresa'] == 0) {
								/*Usuario no valido*/
								$("#usuario").css({"background": "#EFD3D2"});
								$("#msj_usuario").html("Usuario no válido");
								$("#msj_usuario").show();
							} else if (respuesta['ingresa'] == 1) {
								/*Contraseña no valida*/
								$("#clave").css({"background": "#EFD3D2"});
								$("#msj_clave").html("Contraseña no válida");
								$("#msj_clave").show();
							} else if (respuesta['ingresa'] == 2) {
								/*Continuamos*/
								if (respuesta['rol'] == 'lider') {
									window.location = "admin";
								} else if (respuesta['rol'] == 'superadmin' || respuesta['rol'] == 'admin' || respuesta['rol'] == 'capturista') {
									window.location = "lista_planes";
								}
							}
						}
					});
				}
			}
		});


		$("#ingresar").click(function () {
			var avanza = 1;
			var usuario = $("#usuario").val();
			var clave = $("#clave").val();
			if (usuario == '') {
				$("#msj_usuario").html("Ingrese su usuario");
				$("#usuario").css({"background": "#EFD3D2"});
				$("#msj_usuario").show();
				avanza = 0;
			}
			if (clave == '') {
				$("#msj_clave").html("Ingrese su clave");
				$("#clave").css({"background": "#EFD3D2"});
				$("#msj_clave").show();
				avanza = 0;
			}
			if (avanza == 1){
				$.ajax({
					url: 'LoginController/valida',
					data: {
						user: usuario,
						clave: clave
					},
					type: 'POST',
					success: function (response) {
						respuesta = JSON.parse(response);
						if (respuesta['ingresa'] == 0) {
							/*Usuario no valido*/
							$("#usuario").css({"background": "#EFD3D2"});
							$("#msj_usuario").html("Usuario no válido");
							$("#msj_usuario").show();
						} else if (respuesta['ingresa'] == 1) {
							/*Contraseña no valida*/
							$("#clave").css({"background": "#EFD3D2"});
							$("#msj_clave").html("Contraseña no válida");
							$("#msj_clave").show();
						} else if (respuesta['ingresa'] == 2) {
							/*Continuamos*/
							if (respuesta['rol'] == 'lider') {
								window.location = "admin";
							} else if (respuesta['rol'] == 'superadmin' || respuesta['rol'] == 'admin' || respuesta['rol'] == 'capturista') {
								window.location = "lista_planes";
							}
						}
					}
				});
			}
		});
	});
</script>
<body>
<div class="main">


	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center" id="formulario">
				<div class="col-sm-6 col-sm-6 text-center" id="logoimg">
					<img src="assets/build/images/logo.png" class="img-responsive">
				</div>
				<div class="col-sm-6 col-sm-6" id="form1">
					<div class="col-md-12 col-sm-12" style="margin-bottom: 20px;">
						<i class="fa fa-user"></i><input class="datos" type="text" Placeholder="Ingresar usuario" id="usuario" required>
						<div>
							<small id="msj_usuario" style="color: red; display: none;"></small>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<i class="fa fa-lock"></i><input class="datos" type="password" Placeholder="Ingresar contraseña" id="clave" required>
						<div>
							<small id="msj_clave" style="color: red; display: none;"></small>
						</div>
					</div>
					<!--<div class="col-md-12 col-sm-12 text-center" style="margin-top: 25px;">
						<label style="color: #003A5D;">¿Has olvidado tu contraseña?</label>
					</div>-->
					<div class="col-md-12 col-sm-12" style="margin-top: 25px;">
						<button id="ingresar">Ingresar</button>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>
</body>
</html>


