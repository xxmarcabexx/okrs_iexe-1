<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'LoginController';

$route['lista_objetivos'] = 'ObjetivosController';
$route['nuevo_objetivo'] = 'ObjetivosController/alta';
$route['edita_objetivo/(:any)']= 'ObjetivosController/edita/$1';
$route['elimina_objetivo/(:any)']= 'ObjetivosController/elimina/$1';
$route['elimina_kr/(:any)']= 'KeyResultController/elimina/$1';

$route['elimina_plan/(:any)']= 'PlanesController/elimina/$1';



$route['cambioIndicador/(:any)']= 'IndicadoresTempController/cambio/$1';

$route['agrega_key/(:any)']= 'KeyResultController/add/$1';
$route['detalle_objetivo/(:any)']= 'ObjetivosController/detalle/$1';
$route['detalle_plan/(:any)']= 'PlanesController/detalle/$1';
$route['detalle_keyresult/(:any)'] = 'KeyResultController/detalle/$1';
$route['edita_usuario/(:any)'] = 'UsuariosController/editar/$1';

$route['edita_plan/(:any)'] = 'PlanesController/edita/$1';

$route['lista_planes'] = 'PlanesController';
$route['nuevo_plan'] = 'PlanesController/alta';


$route['lista_usuarios'] = 'UsuariosController';
$route['nueva_minuta'] = 'MinutasController/alta';
$route['lista_minutas'] = 'MinutasController';
$route['nueva_accion'] = 'AccionesController/alta';

$route['lista_acciones'] = 'AccionesController';

$route['nuevo_usuario'] = 'UsuariosController/alta';


$route['admin'] = 'AdminController';

$route['acuerdosGob'] = 'MinutasController/gobernador';
$route['edita_acuerdo/(:any)']= 'MinutasController/edita/$1';



$route['404_override'] = 'LoginController/pagenotfound';
$route['translate_uri_dashes'] = FALSE;
