<?php

class AnexosIndicadoresModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "indicadorespdfs";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return true;
		else
			return null;
	}

	public function getByIdKr($idBitacoraKr){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idBitacoraIndicador', $idBitacoraKr);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function aprobar($idBitacora){
		$this->db->set('estatus', '1', FALSE);
		$this->db->where('idBitacoraIndicador', $idBitacora);
		$this->db->update($this->tabla);
		return 1;
	}

	public function rechazar($idBitacora){
		$this->db->set('estatus', '2', FALSE);
		$this->db->where('idBitacoraIndicador', $idBitacora);
		$this->db->update($this->tabla);
		return 1;
	}
	/*public function get(){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->join('rol', "rol.idRol = ".$this->tabla.".idRol");
		$consulta = $this->db->get();
		$resultado = $consulta->result();

		return $resultado;
	}

	public function getUserByClaveUser($data){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('user', $data['user']);
		$this->db->where('clave', $data['clave']);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}*/

	/*public function getByObjetivos($idObjetivo){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idObjetivo', $idObjetivo);
		$consulta = $this->db->get();
		$resultado = $consulta->result();

		return $resultado;
	}


	public function getById($idKr){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idKeyResult', $idKr);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function updateAvance($idKr, $avance){
		$this->db->set('avance', $avance);
		$this->db->where('idKeyResult', $idKr);
		$this->db->update($this->tabla);
		return 1;
	}*/



}
