<?php

class MinutasModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "minuta";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return $this->db->insert_id();
		else
			return null;
	}

	public function get(){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('status', 1);
        $this->db->where('idPlan!=', 0);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

    public function getActivos(){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('status', 1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getById($idMinuta){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('status', 1);
        $this->db->where("idMInuta", $idMinuta);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function setPorcentaje($porcentaje, $idMinuta){
        $this->db->set('avancePorcentaje', $porcentaje);
        $this->db->where('idMinuta', $idMinuta);
        $this->db->update($this->tabla);
    }


    public function getByIdEdit($idMinuta){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->join("plan", "plan.idMv = ".$this->tabla.".idPlan");
        $this->db->where($this->tabla.'.status', 1);
        $this->db->where($this->tabla.".idMInuta", $idMinuta);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }
    public function deleteAllByIdMinuta($idMinuta){
        $this->db->where('idMinuta', $idMinuta);
        $this->db->delete($this->tabla);
    }

    public function delete($idMinuta){
        $this->db->set('status', 0);
        $this->db->where('idMinuta', $idMinuta);
        $this->db->update($this->tabla);
        return 1;
    }

    public function getByIdPlan($idPlan){
	    $this->db->select("*");
	    $this->db->from($this->tabla);
	    $this->db->where("idPlan", $idPlan);
        $this->db->where('status', 1);
        $this->db->where('idPlan!=', 0);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

}
