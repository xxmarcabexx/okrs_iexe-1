<?php

class PdfMinutaModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "pdfminutas";

	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return $this->db->insert_id();
		else
			return null;
	}

	public function get(){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

    public function getByIdMinuta($idMinuta){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('idMinuta', $idMinuta);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }


    public function deletByPdf($pdf){
        $this->db->where('pdf', $pdf);
        $this->db->delete($this->tabla);
    }

    public function deleteAllByIdMunuta($idMinuta){
        $this->db->where('idMinuta', $idMinuta);
        $this->db->delete($this->tabla);
    }




    /*public function getActivos(){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->join('rol', "rol.idRol = ".$this->tabla.".idRol");
        $this->db->where($this->tabla.'.status',1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getUserByClaveUser($data){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('user', $data['user']);
        $this->db->where('clave', $data['clave']);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }
    public function getUser($data){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('user', $data['user']);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function  getByUser($user){
        $this->db->select("*");
        $this->db->from($this->tabla);
        $this->db->where("user", $user);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function update($data, $usuario){
        $this->db->where('user', $usuario);
        $this->db->update($this->tabla, $data);
    }

    public function deleteByUser($user){
        $this->db->where('user', $user);
        $this->db->delete($this->tabla);
    }

    public function delete($user){
        $this->db->set('status', 0);
        $this->db->where('user', $user);
        $this->db->update($this->tabla);
        return 1;
    }*/

}
