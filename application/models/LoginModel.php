<?php

class LoginModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "usuarios";
	}

	public function valida($data){
		$this->db->select("*");
		$this->db->from($this->tabla);
		$this->db->where('user', $data['user']);
		$this->db->where('clave', $data['clave']);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getByUser($user){
		$this->db->select("*");
		$this->db->from($this->tabla);
		$this->db->where('user', $user);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getByUserAndClave($user, $clave){
		$this->db->select("*");
		$this->db->from($this->tabla);
		$this->db->where('user', $user);
		$this->db->where('clave', $clave);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}
	/*public function getById($idMv){
		$this->db->select("*");
		$this->db->from($this->tabla);
		$this->db->where("idMv",$idMv);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}*/
}
