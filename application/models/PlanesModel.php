<?php

class PlanesModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "plan";
	}
	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return $this->db->insert_id();
		else
			return null;
	}

	public function get(){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('estado', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
    }

	public function getById($idPlan){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idMv', $idPlan);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function update($idPlan, $data){
		$this->db->where('idMv', $idPlan);
		if($this->db->update($this->tabla, $data))
			return 1;
		else
			return 0;
	}

	public function deleteById($idMv){
		$this->db->where('idMv', $idMv);
		if($this->db->update($this->tabla, array('estado' => 0)))
			return 1;
		else
			return 0;
	}


	public function totalActualiza($idPlan){
		$this->db->select('plan.idMv, indicadores.idPlan, indicadores.nombreIndicador, bitacoraindicadores.aprobado');
		$this->db->from($this->tabla);
		$this->db->join('indicadores', 'indicadores.idPlan = '.$this->tabla.'.idMv');
		$this->db->join('bitacoraindicadores', "bitacoraindicadores.idIndicador= indicadores.idIndicadores");
		$this->db->where('plan.idMv', $idPlan);
		$this->db->where('bitacoraindicadores.aprobado', 0);
		$this->db->where('indicadores.status', 0);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}


}
