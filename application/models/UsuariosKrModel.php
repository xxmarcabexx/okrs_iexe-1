<?php

class UsuariosKrModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "usuariokeyresult";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}

	public function getKrByUser($user){
		$this->db->select('kr');
		$this->db->from($this->tabla);
		$this->db->where("usuario", $user);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function deleteByUser($user){
		$this->db->where('usuario', $user);
		$this->db->delete($this->tabla);
	}




}
