<?php

class UsuariosPlanesModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "usuarioplanes";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}

	public function getPlanesByUser($user){
		$this->db->select('plan');
		$this->db->from($this->tabla);
		$this->db->where("usuario", $user);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function deleteByUser($user){
		$this->db->where('usuario', $user);
		$this->db->delete($this->tabla);
	}

}
