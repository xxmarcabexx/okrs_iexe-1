<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('PlanesModel');
		$this->load->model('ObjetivosModel');
		$this->load->model('KeyResultModel');
		$this->load->model('IndicadorModel');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
		$this->load->library('session');

	}

	public function index()
	{
		if($this->session->userdata('usuario')!='' || $this->session->userdata('usuario')!= NULL) {
			$dataPlanes = $this->PlanesModel->get();

			$response = $this->ElementosMenu();
			$data = array(
				'planes' => $response,
				'planesGraf' => $dataPlanes
			);
			$this->load->view('dashboardAdmin', $data);
		}else{
			redirect(base_url());
		}
	}




	#Funciones independientes
	public function ElementosMenu(){
		$dataPlanes = $this->PlanesModel->get();
		foreach ($dataPlanes as $planes){
			#Hacemos consulta sobre las key result de ese objetivo
			$dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
			$planes->objetivos = $dataObjetivos;
			foreach ($dataObjetivos as $objetivos){
				$dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
				$objetivos->kr = $dataKeyResult;
			}
		}
		return $dataPlanes;
	}

}
