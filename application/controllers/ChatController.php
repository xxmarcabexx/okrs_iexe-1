<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");
class ChatController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ChatModel');
		$this->load->model('ChatModel');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
		$this->load->library('session');

	}


	public function insert(){
		$data = $this->input->post();
		$data['status']=1;
		$data['fechahora']=date('Y-m-d H:i');
		$result = $this->ChatModel->insert($data);
		echo ($result != null) ? $result : 0;
	}

	public function delete($idTipo){
        $this->ChatModel->delete($idTipo);
    }



}
