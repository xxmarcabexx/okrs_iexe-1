<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class PlanesController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('PlanesModel');
		$this->load->model('ChatModel');
		$this->load->model('IndicadorModel');
		$this->load->model('BitacoraIndicadorModel');
		$this->load->model('IndicadorTempModel');
        $this->load->model('UsuariosPlanesModel');

		$this->load->model('BitacoraMovimientosModel');

		$this->load->model('MvModel');
		$this->load->model('ObjetivosModel');
		$this->load->model('KeyResultModel');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
		$this->load->library('session');

	}

	public function index(){
		$dataChats=array();
		$dataIndicadores=array();
		$totalActualizar = 0;
		if($this->session->userdata('usuario')!='' || $this->session->userdata('usuario')!= NULL){
            $tipo = $this->session->userdata('tipo');
            if ($tipo == 'capturista' ||$tipo == 'admin') {
                $us = $this->session->userdata('idUser');
                $pluser = $this->UsuariosPlanesModel->getPlanesByUser($us);
                foreach ($pluser as $plu) {
                    $pl = $plu->plan;
                    $response = $this->PlanesModel->getById($pl);
                    $dataPlanes[]= $response[0];
                }
                if(isset($dataPlanes)) {

                    foreach ($dataPlanes as $planes) {
                        $dataIndicadores = $this->IndicadorModel->getIndicadoresByIdPlan($planes->idMv);
                        $dataChats = $this->ChatModel->getChatByIdPlan($planes->idMv);
                        $dataTotal = $this->PlanesModel->totalActualiza($planes->idMv);
                        $totalActualizar += count($dataTotal);
                        $planes->chat = $dataChats;
                        $planes->indicadores = $dataIndicadores;
                        foreach ($dataIndicadores as $indc) {
                            $indc->avanceReal = $indc->avance;
                           // $indc->avance = ($indc->avance * 100) / $indc->final;
                            $indc->avance = $indc->avancePorcentaje;

                            #Verificamos si hay algun indicador con avance
                            $dataBitacoraIndicador = $this->BitacoraIndicadorModel->getOneByIdIndicador($indc->idIndicadores);
                            $dataBitacoraIndicadorTemp = $this->IndicadorTempModel->getOneByIdIndicador($indc->idIndicadores);
                            $indc->bitacora = $dataBitacoraIndicador;
                            $indc->bitacoraTemp = $dataBitacoraIndicadorTemp;
                        }
                        $planes->totalActualizar = $totalActualizar;
                        $totalActualizar = 0;
                    }
                    $data = array(
                        'chats' => $dataChats,
                        'planes' => $dataPlanes,
                        'indicadores' => $dataIndicadores,
                        'admin' => false,
                    );
                }else{
                    $data = array(
                        'chats' => null,
                        'planes' => null,
                        'indicadores' => null,
                        'admin' => false,
                    );
                }
                $this->load->view('lista_planes', $data);
            }else {
                $dataPlanes = $this->PlanesModel->get();
                foreach ($dataPlanes as $planes) {
                    #Hacemos consulta sobre los indicadores del plan
                    $dataIndicadores = $this->IndicadorModel->getIndicadoresByIdPlan($planes->idMv);
                    #Hacemos consulta sobre los chat de cada plan
                    $dataChats = $this->ChatModel->getChatByIdPlan($planes->idMv);
                    $dataTotal = $this->PlanesModel->totalActualiza($planes->idMv);
                    $totalActualizar += count($dataTotal);
                    $planes->chat = $dataChats;
                    $planes->indicadores = $dataIndicadores;
                    foreach ($dataIndicadores as $indc) {
                        $indc->avanceReal = $indc->avance;
                        //$indc->avance = ($indc->avance * 100) / $indc->final;
                        $indc->avance = $indc->avancePorcentaje;
                        #Verificamos si hay algun indicador con avance
                        $dataBitacoraIndicador = $this->BitacoraIndicadorModel->getOneByIdIndicador($indc->idIndicadores);
                        $dataBitacoraIndicadorTemp = $this->IndicadorTempModel->getOneByIdIndicador($indc->idIndicadores);
                        $indc->bitacora = $dataBitacoraIndicador;
                        $indc->bitacoraTemp = $dataBitacoraIndicadorTemp;
                    }
                    $planes->totalActualizar = $totalActualizar;
                    $totalActualizar = 0;
                }
                $data = array(
                    'chats' => $dataChats,
                    'planes' => $dataPlanes,
                    'indicadores' => $dataIndicadores,
                    'admin' => false,
                );
                $this->load->view('lista_planes', $data);
            }
		}else{
			redirect(base_url());
		}
	}
	public function getAnual(){
	    $mv = $this->input->post('idmv');
	    #verificamos si existe un objetivo anual
        $objetivoAnual = $this->ObjetivosModel->getAnual($mv);
        if(count($objetivoAnual)>=1){
            echo 1;
        }else{
            echo 0;
        }
    }

	public function alta()
	{
		if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo')!='capturista') {
			$data = array(
				'admin' => false
			);
			$this->load->view('nuevo_plan', $data);
		}else{
			redirect(base_url());
		}
	}

	public function edita($idPlan)
	{
		if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo')!='capturista') {
			$dataPlan = $this->PlanesModel->getById($idPlan);
			$dataPlan[0]->indicadores = $this->IndicadorModel->getIndicadoresByIdPlan($idPlan);
			//var_dump($dataPlan);die;
			$data = array(
				'admin' => false,
				'plan' => $dataPlan
			);
			$this->load->view('edicion_plan', $data);
		}else{
			redirect(base_url());
		}
	}

	public function insert()
	{
		$data = $this->input->post();
		$data['estado']=1;

		$porciones = explode("/", $data['finicial']);
		$data['finicial']= $porciones[2]."-".$porciones[1]."-".$porciones[0];

		$porciones = explode("/", $data['ffinal']);
		$data['ffinal']= $porciones[2]."-".$porciones[1]."-".$porciones[0];

		$result = $this->PlanesModel->insert($data);
		$data = array(
			'movimiento' => 'Alta de nuevo plan',
			'usuario' => $this->session->userdata('idUser'),
			'fecha' => date('Y-m-d'),
			'hora' =>date('H:i')
		);
		$this->BitacoraMovimientosModel->insert($data);
		echo ($result != null) ? $result : 0;
	}


	public function update($idPlan = null)
	{
		if (isset($idPlan)) {
			$data = $this->input->post();

			$porciones = explode("/", $data['finicial']);
			$data['finicial']= $porciones[2]."-".$porciones[1]."-".$porciones[0];

			$porciones = explode("/", $data['ffinal']);
			$data['ffinal']= $porciones[2]."-".$porciones[1]."-".$porciones[0];


			$result = $this->PlanesModel->update($idPlan, $data);
			echo ($result != 0) ? 1 : 0;
		}
	}


	public function detalle($idPlan)
	{
		/*if($this->session->userdata('usuario')!= null ) {*/
		$dataPlan = $this->PlanesModel->getById($idPlan);
		$dataChat = $this->ChatModel->getChatByIdPlan($idPlan);
		$dataObjetivo = $this->ObjetivosModel->getObjetivosByPlanNoAnual($idPlan);
		$dataIndicadores = $this->IndicadorModel->getIndicadoresByIdPlan($idPlan);
		$dataObjetivoAnual = $this->ObjetivosModel->getAnual($idPlan);
		if(count($dataObjetivoAnual)==0){
            $dataObjetivoAnual=0;
            $dataKrAnual = 0;
        }else {
            $dataKrAnual = $this->KeyResultModel->getByObjetivos($dataObjetivoAnual[0]->idObjetivo);
        }

		/*foreach ($dataObjetivo as $objetivos){
		    var_dump($objetivos->idObjetivo);
            $dataKr[] = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
        }*/

		/*$dataMv = $this->MvModel->getById($dataObjetivo[0]->idmv);
    */
		$response = $this->ElementosMenu();
		$data = array(
			'admin' => true,
			'chat' => $dataChat,
			'indicadores' => $dataIndicadores,
			'planes' => $response,
			'pl' => $dataPlan,
			'obj' => $dataObjetivo,
            'objAnual'=>$dataObjetivoAnual,
            'krAnual' => $dataKrAnual
		);
		$this->load->view('detalle_plan', $data);
		/*}else{
			redirect(base_url());
		}*/
	}

	public function elimina($idPlan)
	{
		$response = $this->PlanesModel->deleteById($idPlan);
		$data = array(
			'movimiento'=> 'Eliminacion del plan: '.$idPlan,
			'usuario' =>$this->session->userdata('idUser'),
			'fecha' => date('Y-m-d'),
			'hora' => date('H:i')
		);
		$this->BitacoraMovimientosModel->insert($data);
		echo $response;
	}

	#Funciones independientes
	public function ElementosMenu()
	{
		$dataPlanes = $this->PlanesModel->get();
		foreach ($dataPlanes as $planes) {
			#Hacemos consulta sobre las key result de ese objetivo
			$dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
			$planes->objetivos = $dataObjetivos;
			foreach ($dataObjetivos as $objetivos) {
				$dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
				$objetivos->kr = $dataKeyResult;
			}
		}
		return $dataPlanes;
	}


}
