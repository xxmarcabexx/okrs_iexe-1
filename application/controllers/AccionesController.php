<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccionesController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->model("ObjetivosModel");
        $this->load->model("AccionesModel");
		$this->load->model('KeyResultModel');

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
		$this->load->library('session');
	}


	 public function alta(){
		if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') != 'capturista') {
			$dataKr = $this->KeyResultModel->get();
			$dataObjetivos = $this->ObjetivosModel->getAllAnual();
			$data = array(
				'kr' => $dataKr,
                'objetivo'=>$dataObjetivos
			);
			$this->load->view('nueva_accion', $data);
		} else {
			redirect(base_url());
		}
	}
    public function getById()
    {
        $idAccion = $this->input->post('idAccion');
        $result = $this->AccionesModel->getByIdAccion($idAccion);
        echo json_encode($result);
    }

	public function insert(){
	    $data = $this->input->post();
	    $this->AccionesModel->insert($data);
	    echo 1;
    }

    public function index(){
        $dataAcciones = $this->AccionesModel->getAll();

        $data = array(
	        "acciones"=>$dataAcciones
        );
        $this->load->view('lista_acciones', $data);

    }

    public function delete(){
        $sumaAvanceAcciones = 0;
        $sumaAvanceKr = 0;
	    $idAccion =  $this->input->post('idAccion');
        $this->AccionesModel->deleteAccion($idAccion);
        $dataAcciones = $this->AccionesModel->getByIdAccion($idAccion);
        #Obtenemos la Kr principal
        $krPrincipal = $dataAcciones[0]->idKr;
        #Obtenemos todas las acciones relacionadas con esa kr activas
        $dataAcciones = $this->AccionesModel->getByIdKrActivos($krPrincipal);

        foreach ($dataAcciones as $acciones) {
            #realizamos la suma de los avances de las acciones
            $sumaAvanceAcciones += $acciones->avance;
        }
        #Obtenemos el primedio de las acciones
        if(count($dataAcciones)!= 0 ) {
            $promedioKr = $sumaAvanceAcciones / count($dataAcciones);
        }else{
            $promedioKr = 0;
        }

        #Modificamos el avance de la Kr principal asi como su porcentaje
        $this->KeyResultModel->updateAvance($krPrincipal, $promedioKr);
        $this->KeyResultModel->updateAvancePorcentaje($krPrincipal, $promedioKr);
        #Obtenemos la infomacion de la kr para obtener el objetivo
        $dataKeyResult = $this->KeyResultModel->getById($krPrincipal);
        $objetivoPrincipal = $dataKeyResult[0]->idObjetivo;
        $dataKeyResult = $this->KeyResultModel->getByIdObj($objetivoPrincipal);
        foreach ($dataKeyResult as $kr) {
            #realizamos la suma de los avances de los OKR
            $sumaAvanceKr += $kr->avancePorcentaje;
        }
        if(count($dataKeyResult)!=0) {
            $promedioObjetivo = $sumaAvanceKr / count($dataKeyResult);
        }else{
            $promedioObjetivo = 0;
        }
        $dataUpdateObj = array(
            "avance" => $promedioObjetivo,
            "avancePorcentaje" => $promedioObjetivo
        );
        $this->ObjetivosModel->update($objetivoPrincipal, $dataUpdateObj);
        $dataResponse = array(
            "promedio" => $promedioKr,
            "promedioObjetivo" => $promedioObjetivo,
            "objetivo" => $objetivoPrincipal,
            "kr" => $krPrincipal
        );
        echo json_encode($dataResponse);
    }




}
