<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IndicadoresController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('IndicadorModel');
		$this->load->model('PlanesModel');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
	}

	public function insert(){
		$data = json_decode($this->input->post('obj'));
		foreach ($data as $indicador){
			$dataInsert = array(
				'nombreIndicador' => $indicador->nombreIndicador,
				'inicio'=> $indicador->inicio,
				'final'=> $indicador->final,
				'avance'=> $indicador->avance,
				'idPlan'=> $indicador->idPlan
			);
			$result = $this->IndicadorModel->insert($dataInsert);
		}

		echo ($result != null) ? $result : 0;

	}


	public function getById(){
		$data = $this->input->post('idIndicador');
		$result = $this->IndicadorModel->getById($data);
		echo json_encode($result);

	}

	public function getPlanByIdIndicador(){
		$data = $this->input->post('idIndicador');
		$result = $this->IndicadorModel->getById($data);
		$idPlan = $result[0]->idPlan;
		$dataIndicador = $this->IndicadorModel->ObtienePromedioByPlan($idPlan);
		$promedio = $dataIndicador[0]->promedio;
		$dataUpdate =array(
			"avanceIndicadores" => $promedio
		);
		$this->PlanesModel->update($idPlan, $dataUpdate);

		$dataPlanes = $this->PlanesModel->getById($idPlan);
		echo json_encode($dataPlanes);
	}

	public function editarAvance(){
		$data = $this->input->post();
		$dataOperacion= $this->IndicadorModel->getById($data['idIndicador']);
		$porcentaje = ($data['avance']*100)/$dataOperacion[0]->final;
		$response = $this->IndicadorModel->updateAvance($data['idIndicador'], $data['avance'], $porcentaje);

		echo $porcentaje;
	}

	public function edit($idIndicador){
		var_dump($idIndicador);
	}


	public function update($idPlan = null){
		if(isset($idPlan)) {
			$data = $this->input->post();
			$result = $this->PlanesModel->update($idPlan, $data);
			echo ($result != 0) ? 1 : 0;
		}
	}

	/*


	public function detalle($idObjetivo){
		$response = $this->objetivosMenu();
		$dataObjetivo = $this->ObjetivosModel->getById($idObjetivo);
		$dataKeyResult = $this->KeyResultModel->getByObjetivos($idObjetivo);
		$dataMv = $this->MvModel->getById($dataObjetivo[0]->idmv);

		$data = array(
			'admin' => true,
			'objetivos' => $response,
			'objetivo' => $dataObjetivo,
			'keyresult' => $dataKeyResult,
			'mv'=> $dataMv
		);
		$this->load->view('detalle_objetivo', $data);
	}


	#Funciones independientes
	public function objetivosMenu(){
		$dataObjetivos = $this->ObjetivosModel->get();
		foreach ($dataObjetivos as $objetivos){
			#Hacemos consulta sobre las key result de ese objetivo
			$dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
			$progresoIndividual = 0;
			if(count($dataKeyResult) > 0) {
				foreach ($dataKeyResult as $kr) {
					if ($kr->metrica != 'Porcentaje') {
						$progreso = (($kr->avance) * 100) / $kr->medicionfinal;
					} else {
						$progreso = $kr->avance;
					}
					$kr->progreso = $progreso;
					$progresoIndividual = $progresoIndividual + $progreso;
				}

				$progresoObjetivo = ($progresoIndividual * 100) / (count($dataKeyResult) * 100);
				$objetivos->progreso = $progresoObjetivo;
			}else{
				$objetivos->progreso = 0;
			}
			$objetivos->kr = $dataKeyResult;
		}

		return $dataObjetivos;
	}*/



}
