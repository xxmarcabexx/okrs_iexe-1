<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class LoginController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('LoginModel');
		$this->load->model('SesionModel');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index()
	{
            $this->session->unset_userdata('usuario');
            $this->session->unset_userdata('tipo');
            $this->session->unset_userdata('idUser');
            $this->session->unset_userdata('rol');
            $this->load->view('login');


	}

	public function valida()
	{
		$data = $this->input->post();
		$datosUsua  = $this->LoginModel->getByUser($data['user']);
		if (count($datosUsua) > 0) {
			$response = $this->LoginModel->valida($data);
			if (count($response) > 0) {
				#Existe registro
				$tipo = $response[0]->idRol;
				switch ($tipo) {
					#Lider
					case 1:
						$rol = "lider";
						break;
					#Super admin
					case 2:
						$rol = "superadmin";
						break;
					#Admin
					case 3:
						$rol = "admin";
						break;
					#Capturista
					case 4:
						$rol = "capturista";
						break;
				}
				$ip = $this->ObtenerIP();
				$arraydata = array(
					'usuario' => $response[0]->nombre,
					'tipo' => $rol,
					'idUser' => $response[0]->user,
                    'idRol' => $response[0]->idRol
				);
				$data = array(
					'usuario' => $response[0]->user,
					'fecha' => date("Y-m-d H:i"),
					'tipo' => 'Inicio de Sesion',
					'ip' => $ip
				);
				$this->SesionModel->insert($data);
				$this->session->set_userdata($arraydata);

				$respuesta = array(
					"rol"=> $rol,
					"ingresa"=> 2
				);
				echo json_encode($respuesta);
			} else {
				$respuesta = array(
					"ingresa"=> 1
				);
				echo json_encode($respuesta);
			}
		}else{
			#Usuario no valido
			$respuesta = array(
				"ingresa"=> 0
			);
			echo json_encode($respuesta);
		}


	}


	public function pagenotfound()
	{
		$this->load->view('pagenotfound');
	}


	function ObtenerIP()
	{
		$ip = "";
		if(isset($_SERVER))
		{
			if (!empty($_SERVER['HTTP_CLIENT_IP']))
			{
				$ip=$_SERVER['HTTP_CLIENT_IP'];
			}
			elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			else
			{
				$ip=$_SERVER['REMOTE_ADDR'];
			}
		}
		else
		{
			if ( getenv( 'HTTP_CLIENT_IP' ) )
			{
				$ip = getenv( 'HTTP_CLIENT_IP' );
			}
			elseif( getenv( 'HTTP_X_FORWARDED_FOR' ) )
			{
				$ip = getenv( 'HTTP_X_FORWARDED_FOR' );
			}
			else
			{
				$ip = getenv( 'REMOTE_ADDR' );
			}
		}
		// En algunos casos muy raros la ip es devuelta repetida dos veces separada por coma
		if(strstr($ip,','))
		{
			$ip = array_shift(explode(',',$ip));
		}
		return $ip;
	}


}
